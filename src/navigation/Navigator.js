import React from 'react';

//Navigation Import
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
const Stack = createNativeStackNavigator();

// Screen import
import TabNavigator from './TabNavigator';
import { IdentityVerification, InitialScreen } from '../screens';
import LoginScreen from '../screens/OtherScreens/LoginScreen/LoginScreen';
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen/ForgotPasswordScreen';
import ResetPasswordScreen from '../screens/OtherScreens/ResetPasswordScreen/ResetPasswordScreen';
import GoBackLogin from '../screens/OtherScreens/GoBackLogin/GoBackLogin';
import PersonalInformation from '../screens/RegistrationScreen/PersonalInformation/PersonalInformation';
import MedicalRegistrationDocument from '../screens/RegistrationScreen/MedicalRegistrationDocument/MedicalRegistrationDocument';
import MedicalRegistrationDocument3 from '../screens/RegistrationScreen/MedicalRegistrationDocument3/MedicalRegistrationDocument3';
import MedicalRegistrationDocument4 from '../screens/RegistrationScreen/MedicalRegistrationDocument4/MedicalRegistrationDocument4';
import MedicalRegistrationDocument2 from '../screens/RegistrationScreen/MedicalRegistrationDocument2/MedicalRegistrationDocument2';
import LoginWithPin from '../screens/OtherScreens/LoginWithPin/LoginWithPin';
import RatingScreen from '../screens/RatingScreen/RatingScreen';
import PermissionScreen from '../screens/PermissionScreen/PermissionScreen';
import AboutusScreen from '../screens/AboutusScreen/AboutusScreen';
import IntroLogin from '../screens/OtherScreens/IntroLogin/IntroLogin';
import HelpScreen from '../screens/HelpScreen/HelpScreen';
import AppointmentDetailsScreen from '../screens/AppointmentDetails/AppointmentDetailsScreen';
import CancelScreen from '../screens/CancelScreen/CancelScreen';
import ChangeLocationScreen from '../screens/ChangeLocation/ChangeLocation';
import HospitalDetailsScreen from '../screens/HospitalDetails/HospitalDetailsScreen';
import BookingConfirmScreen from '../screens/BookingConfirm/BookingConfirmScreen';
import SearchListScreen from '../screens/SearchList/SearchListScreen';
import TermsScreen from '../screens/TermsScreen/TermsScreen';
import CongratulationScreen from '../screens/CongratulationScreen/CongratulationScreen';
import EarningDetailsScreen from '../screens/EarningDetails/EarningDetailsScreen';
import IndividualEarningScreen from '../screens/IndividualEarning/IndividualEarningScreen';
import LocationScreen from '../screens/RegistrationScreen/Location/Location';
import BankInformation from '../screens/RegistrationScreen/BankInformation/BankInformation';
import SetPasswordScreen from '../screens/SetPassword/SetPasswordScreen';
import VerifyotpScreen from '../screens/VerifyotpScreen/VerifyotpScreen';
import ProfileScreen from '../screens/ProfileScreen/ProfileScreen';
import PermissionInfoScreen from '../screens/PermissionInfo/PermissionInfoScreen';
import ChooseUserTypeScreen from '../screens/ChooseUsertype/ChooseUserTypeScreen';
import WalletScreen from '../screens/WalletScreen/WalletScreen';
// import {
//     OrderHistory
// } from '../screens'

function Navigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator>

        {/* == InitialScreen == 
    This screen always should be in the TOP, 
    DO NOT CHANGE THIS SCREEN POSITION */}
        <Stack.Screen
          name="Initial"
          component={InitialScreen}
          options={{ headerShown: false }}
        />

        {/* == TabNavigator == 
    This screen USED FOT TAB PURPOSE, 
    DO NOT CHANGE THIS SCREEN POSITION */}
        <Stack.Screen
          name="TabNavigator"
          component={TabNavigator}
          options={{ headerShown: false }}
        />

        {/* New screens */}
        <Stack.Screen
          name="AppointmentDetailsScreen"
          component={AppointmentDetailsScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="CancelScreen"
          component={CancelScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ChangeLocationScreen"
          component={ChangeLocationScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="HospitalDetailsScreen"
          component={HospitalDetailsScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="BookingConfirmScreen"
          component={BookingConfirmScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SearchListScreen"
          component={SearchListScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="TermsScreen"
          component={TermsScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="CongratulationScreen"
          component={CongratulationScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="EarningDetailsScreen"
          component={EarningDetailsScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="IndividualEarningScreen"
          component={IndividualEarningScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="LocationScreen"
          component={LocationScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="BankInformation"
          component={BankInformation}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SetPasswordScreen"
          component={SetPasswordScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="VerifyotpScreen"
          component={VerifyotpScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="PermissionScreen"
          component={PermissionScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ProfileScreen"
          component={ProfileScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="PermissionInfoScreen"
          component={PermissionInfoScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ChooseUserTypeScreen"
          component={ChooseUserTypeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="WalletScreen"
          component={WalletScreen}
          options={{ headerShown: false }}
        />
        


        
        










        {/* New screens */}

        <Stack.Screen
          name="IntroLogin"
          component={IntroLogin}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="AboutusScreen"
          component={AboutusScreen}
          options={{ headerShown: false }}
        />

        {/* new screens */}
        {/* new screens */}

        <Stack.Screen
          name="HelpScreen"
          component={HelpScreen}
          options={{ headerShown: false }}
        />
        {/* <Stack.Screen
          name="IntroLogin"
          component={IntroLogin}
          options={{headerShown: false}}
        /> */}

        <Stack.Screen
          name="LoginWithPin"
          component={LoginWithPin}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="IdentityVerification"
          component={IdentityVerification}
          options={{ headerShown: false }}
        />




        <Stack.Screen
          name="RatingScreen"
          component={RatingScreen}
          options={{ headerShown: false }}
        />


        <Stack.Screen
          name="ForgotPassword"
          component={ForgotPasswordScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="PersonalInformation"
          component={PersonalInformation}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MedicalRegistrationDocument4"
          component={MedicalRegistrationDocument4}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MedicalRegistrationDocument2"
          component={MedicalRegistrationDocument2}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MedicalRegistrationDocument3"
          component={MedicalRegistrationDocument3}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="MedicalRegistrationDocument"
          component={MedicalRegistrationDocument}
          options={{ headerShown: false }}
        />



        <Stack.Screen
          name="GoBackLogin"
          component={GoBackLogin}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ResetPasswordScreen"
          component={ResetPasswordScreen}
          options={{ headerShown: false }}
        />



        <Stack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{ headerShown: false }}
        />


        {/* <Stack.Screen
                    name="OrderHistory"
                    component={Order}
                    options={{ headerShown: true }}
                /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Navigator;
