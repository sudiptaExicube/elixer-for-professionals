// import { View, Text, StyleSheet } from 'react-native'
// import React from 'react'
// import { Theme } from '../../../constants'

// const Style = () => {

//   return (
//     StyleSheet.create({
//  createAccountStyles:{
//       fontFamily: Theme.FontFamily.normal,
//           fontStyle: 'normal',
//           fontWeight: '500',
//           fontSize: 12,
//           lineHeight: 15,
//           textAlign: 'center',
//           color: '#000',
//           paddingVertical:5
          
//         },
//         createAccountTextStyles:{
//           fontFamily: Theme.FontFamily.normal,
//         fontStyle: 'normal',
//         fontWeight: '500',
//         fontSize: 12,
//         lineHeight: 15,
//         textAlign: 'center',
//         color: '#BFA058',
//         paddingVertical:5
//         },
//         text:{
//             fontFamily: Theme.FontFamily.normal,
//             fontStyle: 'normal',
//             fontWeight: '500',
//             fontSize: 13,
//             lineHeight: 39,
//             letterSpacing: -0.333333,
//             textTransform: 'capitalize',
//             color: Theme.colors.black,
//           },
//           newUserText: {
//             fontFamily: Theme.FontFamily.normal,
//             fontStyle: 'normal',
//             fontWeight: '500',
//             fontSize: 12,
//             lineHeight: 15,
//             textAlign: 'center',
//             color: '#000',
//           },
//     })
//   )
// }

// export default Style


import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import { Theme } from '../../../constants'

const Style = () => {

  return (
    StyleSheet.create({
 createAccountStyles:{
      fontFamily: Theme.FontFamily.normal,
          fontStyle: 'normal',
          fontWeight: '500',
          fontSize: 12,
          lineHeight: 15,
          textAlign: 'center',
          color: '#000',
          paddingVertical:5
          
        },
        createAccountTextStyles:{
          fontFamily: Theme.FontFamily.normal,
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 15,
        textAlign: 'center',
        color: '#BFA058',
        paddingVertical:5
        },
        text:{
            fontFamily: Theme.FontFamily.normal,
            fontStyle: 'normal',
            fontWeight: '500',
            fontSize: 13,
            lineHeight: 39,
            letterSpacing: -0.333333,
            textTransform: 'capitalize',
            color: Theme.colors.black,
          },
          newUserText: {
            fontFamily: Theme.FontFamily.normal,
            fontStyle: 'normal',
            fontWeight: '500',
            fontSize: 12,
            lineHeight: 15,
            textAlign: 'center',
            color: '#000',
          },
    })
  )
}

export default Style