import {View, Text, TouchableOpacity} from 'react-native';
import React, {useRef, useState} from 'react';
import {
  BlackButton,
  DatePicker,
  DropdownPicker,
  EditTextInputBox,
  MyDatePicker,
  ScreenLayout,
} from '../../../Components';
import {Theme} from '../../../constants';
import {windowHeight, windowWidth} from '../../../constants/GlobalStyles';
import Style from './Style';
import UploadImageBox from '../../../Components/UploadImageBox/UploadImageBox';
import ModalBottom from '../../../Components/ModalBottom/ModalBottom';

const Styles = Style();
const MedicalRegistrationDocument4 = (props) => {
  const [colageName, setCollageName] = useState(null);
  const [graduationStartYear,setGraduationStartYear]=useState(new Date())
  const [graduationEndYear,setGraduationEndYear]=useState(new Date())
  const [graduationStartYearInit,setGraduationStartYearInit]=useState("Year Appeared")
  const [graduationEndYearInit,setGraduationEndYearInit]=useState("Year Passed")
  const viewRef = useRef(null);
  const [documentBottomVisible,setDocumentBottomVisible]=useState(false)
  const [selectedDocumentType,setSelectedDocumentType]=useState("")
  
  const closeDocumentType=()=>{
    setDocumentBottomVisible(false)
  }
  
  const showBounceAnimation = value => {
    value.current.animate({
      0: {scale: 1, rotate: '0deg'},
      1: {scale: 1.7, rotate: '0deg'},
    });
    value.current.animate({
      0: {scale: 1.7, rotate: '0deg'},
      1: {scale: 1, rotate: '0deg'},
    });
  };
  return (
    <ScreenLayout
      isHeaderShown={true}
      isHeaderLogo={false}
      hTitle={'Medical Registration Document'}
      headerBackground={Theme.colors.white}
      headerStyle={{width: '100%', alignSelf: 'center'}}
      showHeaderLine={false}
      customBackground={Theme.colors.secondaryBackground}
      isBackBtn={true}
      backBtnIcnName={'chevron-back-outline'}
      backBtnFunc={() => {
        showBounceAnimation(viewRef);
        props.navigation.goBack();
      }}
      backButton_animationRef={viewRef}>
      <View
        style={{
          flex: 1,
          backgroundColor: Theme.colors.white,
          height: windowHeight - 50,
          width: windowWidth,
        }}>
        <View
          style={{
            flexDirection: 'row',
            width: '87%',
            alignSelf: 'center',
            marginTop: '5%',
          }}>
          <Text style={Styles.Undergraduate}> Fellowship/ Certification Course :</Text>
          <TouchableOpacity 
            onPress={()=>{props.navigation.navigate('BankInformation')}}
            style={{end: 0, position: 'absolute'}}>
            <Text
              style={{
                fontFamily: Theme.FontFamily.normal,
                fontStyle: 'normal',
                fontWeight: '500',
                fontSize: 15,
                lineHeight: 18,
                textAlign: 'center',
                letterSpacing: -0.333333,
                color: '#ABABAB',
                justifyContent: 'flex-end',
              
              }}>
              Skip
            </Text>
          </TouchableOpacity>
        </View>
              {/* <DropdownPicker style={{marginTop:'7%'}} allValues={["College Name"]}/> */}
              <DropdownPicker 
                selectedValue={'Course Name'} 
                setDocumentBottomVisible={()=>{
                  setDocumentBottomVisible(true)
                  setSelectedDocumentType("Course Name")
                }} 
                style={{marginTop:'7%'}}
                
              />

              <ModalBottom modalCloseAction={()=>{closeDocumentType()}} 
                modalHeaderText={selectedDocumentType} 
                modalVisible={documentBottomVisible}>
                <View style={{paddingTop:40}}>
                  <Text style={{fontSize:Theme.sizes.h5,lineHeight:25,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Data should be dynamic, and We will implement in the time of development phase</Text>
                </View>
              </ModalBottom>
              
              <View style={{marginTop:'7%',flexDirection:'row',width:'87%',alignSelf:'center',height:'7%'}}>
                
                <MyDatePicker firstTime={graduationStartYearInit} setFirstTime={setGraduationStartYearInit} setDate={setGraduationStartYear} date={graduationStartYear} style={{width:'45%',height:'100%'}}/>
                <MyDatePicker firstTime={graduationEndYearInit} setFirstTime={setGraduationEndYearInit} setDate={setGraduationEndYear} date={graduationEndYear} style={{width:'45%',position:'absolute',end:0,height:'100%'}}/>
              </View>
              <EditTextInputBox placeHolderText="Registration Number *" style={{marginTop:'7%'}} />
              <Text style={Styles.uploadDocument}>Upload Document Image</Text>

              <View style={{height:'20%',flexDirection:'row',width:'100%',justifyContent:'space-around'}}>
                <UploadImageBox lebel="Front image (max 5 mb)" imageClick={()=>{alert("Camera will implement in the time of functionality development (Developemnt phase")}} />
                <UploadImageBox lebel="Back image (max 5 mb)" imageClick={()=>{alert("Camera will implement in the time of functionality development (Developemnt phase")}}/>
              </View>
              <BlackButton buttonText="NEXT" onSubmitPress={()=>{props.navigation.navigate('BankInformation')}} style={{marginTop:'7%'}}/>
        {/* <View
          style={{
            flexDirection: 'row',
            // position: 'absolute',
            // bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 15,
            width: '100%',
            paddingTop:10
          }}>
          <Text style={Styles.newUserText}>Already have account? </Text>
          <TouchableOpacity
            style={Styles.createAccountStyles}
            onPress={() => {props.navigation.replace('LoginScreen')}}>
            <Text style={Styles.createAccountTextStyles}>Login</Text>
          </TouchableOpacity>
        </View> */}
      </View>
    </ScreenLayout>
  );
};

export default MedicalRegistrationDocument4;
