import { View, Text, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { BlackButton, EditTextInputBox, ScreenLayout } from '../../../Components'
import { useRef } from 'react';
import { Theme } from '../../../constants';
import Style from './Style';
import { windowHeight, windowWidth } from '../../../constants/GlobalStyles';
import { useState } from 'react';
import { CheckBox } from '@rneui/themed';
import { color } from '@rneui/base';
import { avatarSizes } from '@rneui/base/dist/Avatar/Avatar';

const Styles=Style();

const 
PersonalInformation = (props) => {
  const [Gender, setGenderCheckBox] = useState(null)


  const viewRef = useRef(null);
  const showBounceAnimation = (value) => {
    value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
    value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
  }

  return (
    <ScreenLayout 
    isHeaderShown={true}
    isHeaderLogo={false}
    hTitle={"Personal Information"}
    headerBackground={Theme.colors.white}
    headerStyle={{width: '100%',alignSelf: 'center'}}
    showHeaderLine={false}
    customBackground={Theme.colors.secondaryBackground}
    isBackBtn={true}
    backBtnIcnName={"chevron-back-outline"}
    backBtnFunc={() => {
      showBounceAnimation(viewRef)
      props.navigation.goBack()
    }}
    backButton_animationRef={viewRef}
    >
<View style={{flex:1,backgroundColor:Theme.colors.white,height:windowHeight-50,width:windowWidth}}>

<EditTextInputBox placeHolderText="First Name" style={{marginTop:'8%'}}/>
<EditTextInputBox placeHolderText="Last Name" style={{marginTop:'8%'}}/>
<View style={{flexDirection:'row',alignItems:'center',width:'85%',alignSelf:'center',marginTop:'8%'}}>
  <Text style={Styles.text}>Gender</Text>
<CheckBox
        marginStart='19%'
        marginEnd='-1%'
           checked={Gender===0}
           onPress={()=>setGenderCheckBox(0)}
           checkedColor='#000'
           uncheckedColor='#000'
           iconType="material-community"
           checkedIcon="checkbox-outline"
           uncheckedIcon={'checkbox-blank-outline'}
           size={18}
         />
  <Text style={Styles.text}>Male</Text> 
  <Image style={{marginStart:'2%'}} source={require("../../../assets/images/MaleIcon.png")}/>

    <CheckBox
          marginEnd='-1%'
           checked={Gender===1}
           onPress={()=>setGenderCheckBox(1)}
           checkedColor='#000'
           uncheckedColor='#000'
           iconType="material-community"
           checkedIcon="checkbox-outline"
           uncheckedIcon={'checkbox-blank-outline'}
           size={18}
         />
    <Text style={Styles.text}>Female</Text>
  <Image style={{marginStart:'2%'}} source={require("../../../assets/images/FemaleIcon.png")}/>
</View>
<EditTextInputBox keyboardType={'email-address'} placeHolderText="Email" style={{marginTop:'8%'}}/>
<EditTextInputBox keyboardType={'numeric'} placeHolderText="Phone No" style={{marginTop:'8%'}}/>
<BlackButton style={{ marginTop: '13%' }} buttonText="SAVE" 
// onSubmitPress={()=>{props.navigation.navigate('IdentityVerification')}} 
onSubmitPress={()=>{props.navigation.navigate('VerifyotpScreen')}} 
/>
{/* <View style={{ flexDirection: 'row', justifyContent: 'center',paddingTop:10,
  alignItems: 'center',marginBottom:15,width:'100%' }}>
  <Text style={Styles.newUserText}>Already have account? </Text>
  <TouchableOpacity
    style={Styles.createAccountStyles}
    onPress={()=>{props.navigation.replace('LoginScreen')}}
  >
    <Text
      style={Styles.createAccountTextStyles}
    >Login</Text>
  </TouchableOpacity>
</View> */}
</View>
    </ScreenLayout>
  )
}

export default PersonalInformation