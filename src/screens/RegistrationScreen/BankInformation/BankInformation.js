import { View, Text, TouchableOpacity } from 'react-native'
import React, { useRef, useState } from 'react'
import { BlackButton, DropdownPicker, EditTextInputBox, ScreenLayout } from '../../../Components'
import ModalBottom from '../../../Components/ModalBottom/ModalBottom'
import { Theme } from '../../../constants'
import { windowHeight, windowWidth } from '../../../constants/GlobalStyles'
import Style from './Style'

const Styles=Style();

const BankInformation = props => {
    const [bankNameBottomVisible,setBankNameBottomVisible]=useState(false)
    const [selectedBankName,setSelectedBankName]=useState("Bank Name")
      const viewRef = useRef(null);
      const showBounceAnimation = (value) => {
        value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
        value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
      }
      const [documentBottomVisible,setDocumentBottomVisible]=useState(false)
      const [selectedDocumentType,setSelectedDocumentType]=useState("")
      
      const closeDocumentType=()=>{
        setDocumentBottomVisible(false)
      }
    
      const closeBankName=()=>{
        setBankNameBottomVisible(false)
      }
  return (
    <ScreenLayout
    isHeaderShown={true}
      isHeaderLogo={false}
      hTitle={'Bank Information'}
      headerBackground={Theme.colors.white}
      headerStyle={{width: '100%', alignSelf: 'center'}}
      showHeaderLine={false}
      customBackground={Theme.colors.secondaryBackground}
      isBackBtn={true}
      backBtnIcnName={'chevron-back-outline'}
      backBtnFunc={() => {
        showBounceAnimation(viewRef);
        props.navigation.goBack()
      }}
      backButton_animationRef={viewRef}
    >
        <View style={{flex:1,backgroundColor:Theme.colors.white,height:windowHeight,width:windowWidth}}>
        {/* <DropdownPicker selectedValue={selectedBankName} setDocumentBottomVisible={setSelectedBankName} style={{marginTop:'10%'}}/> */}
        <DropdownPicker 
          selectedValue={'Bank Name'} 
          setDocumentBottomVisible={()=>{
            setDocumentBottomVisible(true)
            setSelectedDocumentType("Bank Name")
          }} 
          style={{marginTop:'7%'}}
          
        />
        <ModalBottom modalCloseAction={()=>{closeDocumentType()}} 
          modalHeaderText={selectedDocumentType} 
          modalVisible={documentBottomVisible}>
          <View style={{paddingTop:40}}>
            <Text style={{fontSize:Theme.sizes.h5,lineHeight:25,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Data should be dynamic, and We will implement in the time of development phase</Text>
          </View>
        </ModalBottom>

        <EditTextInputBox placeHolderText="Bank Address" style={{marginTop:'9%'}}/>
        <EditTextInputBox placeHolderText="Branch Code*" style={{marginTop:'9%'}}/>
        <EditTextInputBox keyboardType={'numeric'} placeHolderText="Account Number*" style={{marginTop:'9%'}}/>
        <EditTextInputBox placeHolderText="IFSC Code*" style={{marginTop:'9%'}}/>
        {/* <ModalBottom modalCloseAction={closeBankName} modalHeaderText="Select Bank Name" modalVisible={bankNameBottomVisible}/> */}
        <View style={{marginHorizontal:25,marginTop:'10%',borderWidth:1,padding:10,borderColor:Theme.colors.secondary,borderRadius:10,backgroundColor:'#F3F3F3'}}>
        <Text style={{fontSize:Theme.sizes.h5, fontFamily:Theme.FontFamily.bold,color:Theme.colors.secondary,textAlign:'center',paddingBottom:5}}>
            Disclaimer
          </Text>
          <Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.secondary,textAlign:'center'}}>
              Chhaya health won’t be held responsible for any wrong entry in above bank details. 
          </Text>
        </View>
        
        <BlackButton style={{ marginTop: '13%' }} buttonText="SAVE" onSubmitPress={()=>{props.navigation.navigate('LocationScreen')}} />
      {/* <View style={{ flexDirection: 'row', paddingTop:10, alignSelf: 'center', alignItems: 'center' }}>
          <Text style={Styles.newUserText}>Already have account? </Text>
          <TouchableOpacity
            style={Styles.createAccountStyles}
            onPress={()=>{props.navigation.replace('LoginScreen')}}
          >
            <Text
              style={Styles.createAccountTextStyles}
            >Login</Text>
          </TouchableOpacity>
        </View> */}
        {/* <View style={{paddingHorizontal:25,marginTop:'10%'}}>
          <Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.primary,textAlign:'center'}}>
              Chhaya health won’t be held responsible for any wrong entry in above bank details. 
          </Text>
        </View> */}
</View>
    </ScreenLayout>
  )
}

export default BankInformation;