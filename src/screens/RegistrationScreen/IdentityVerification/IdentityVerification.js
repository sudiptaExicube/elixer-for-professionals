import { View, Text, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import { BlackButton, DropdownPicker, EditTextInputBox, ScreenLayout } from '../../../Components'
import { Theme } from '../../../constants'
import { useRef } from 'react'
import { windowHeight, windowWidth } from '../../../constants/GlobalStyles'
import { Picker } from '@react-native-picker/picker'
import UploadImageBox from '../../../Components/UploadImageBox/UploadImageBox'
import Style from './style'
import { ThemeContext } from '../../../constants/ThemeContext'
import ModalBottom from '../../../Components/ModalBottom/ModalBottom'

const Styles=Style()

const IdentityVerification = props => {
const [documentBottomVisible,setDocumentBottomVisible]=useState(false)
const [selectedDocumentType,setSelectedDocumentType]=useState("Document Type")
  const viewRef = useRef(null);
  const showBounceAnimation = (value) => {
    value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
    value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
  }

  const closeDocumentType=()=>{
      setDocumentBottomVisible(false)
  }
  

  return (
    <ScreenLayout
    isHeaderShown={true}
    isHeaderLogo={false}
    hTitle={"Identity Verification"}
    headerBackground={Theme.colors.white}
    headerStyle={{width: '100%',alignSelf: 'center'}}
    showHeaderLine={false}
    customBackground={Theme.colors.secondaryBackground}

    isBackBtn={true}
    backBtnIcnName={"chevron-back-outline"}
    backBtnFunc={() => {
      showBounceAnimation(viewRef)
      props.navigation.goBack();
    }}
    backButton_animationRef={viewRef}
    >
      <View style={{flex:1,backgroundColor:Theme.colors.white,height:windowHeight,width:windowWidth}}>
        <DropdownPicker selectedValue={selectedDocumentType} setDocumentBottomVisible={()=>{setDocumentBottomVisible(true)}} style={{marginTop:'10%'}}/>
        <EditTextInputBox placeHolderText="Input document number" style={{marginTop:'8%'}}/>
        <View style={{flexDirection:'row',height:'20%',alignItems:'center',justifyContent:'space-around',marginVertical:'8%'}}>
          <Text style={{
    fontFamily: Theme.FontFamily.normal,
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 12,
    lineHeight: 39,
    letterSpacing: -0.333333,
    textTransform: 'capitalize',
    color: '#263238'
  }}> Upload document image</Text>
          <UploadImageBox />
        </View>
      <EditTextInputBox placeHolderText="PAN Card Number "/>

      <BlackButton style={{ marginTop: '13%' }} buttonText="SAVE" onSubmitPress={()=>{props.navigation.navigate('MedicalRegistrationDocument')}} />
      {/* <View style={{ flexDirection: 'row', marginTop:20, alignSelf: 'center', alignItems: 'center' }}>
          <Text style={Styles.newUserText}>Already have account? </Text>
          <TouchableOpacity
            style={Styles.createAccountStyles}
            onPress={()=>{props.navigation.replace('LoginScreen')}}
          >
            <Text
              style={Styles.createAccountTextStyles}
            >Login</Text>
          </TouchableOpacity>
        </View> */}
        <ModalBottom modalCloseAction={closeDocumentType} 
        modalHeaderText="Select Document Type" 
        modalVisible={documentBottomVisible}>
          <View style={{paddingTop:40}}>
            <Text style={{fontSize:Theme.sizes.h5,lineHeight:25,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Data should be dynamic, and We will implement in the time of development phase</Text>
          </View>
          </ModalBottom>
      </View>

    </ScreenLayout>
  )
}

export default IdentityVerification