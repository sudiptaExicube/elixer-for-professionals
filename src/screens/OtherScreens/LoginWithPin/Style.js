// //import liraries
// import React, {Component} from 'react';
// import {StyleSheet} from 'react-native';
// import {useTheme} from '../../../constants/ThemeContext';
// import {windowHeight} from '../../../constants/GlobalStyles';
// import {Theme} from '../../../constants';

// // create a component
// const Style = () => {
//   const {colorTheme} = useTheme;
//   return StyleSheet.create({
//     container: {
//       flex: 1,
//     },
//     yellowLine: {
//       width: '100%',
//       height: 5,
//       backgroundColor: Theme.colors.primary,
//       borderColor: Theme.colors.black,
//       borderWidth: 1,
//       shadowColor: Theme.colors.black,
//       shadowOffset: {width: 1, height: 3},
//       shadowOpacity: 0.2,
//       shadowRadius: 1,
//     },
//     containerBox: {
//       backgroundColor: Theme.colors.white,
//       height: windowHeight - 5,
//       width: '94%',
//       // borderTopColor: Theme.colors.white,
//       // borderWidth: 1,
//       alignSelf: 'center',
//       elevation: 4,
//       shadowColor: 'rgba(0, 0, 0, 0.25)',
//       shadowOffset: {width: 0, height: 4},
//       shadowOpacity: 1,
//       shadowRadius: 4
//     },
//     loginText: {
//       fontFamily: Theme.FontFamily.normal,
//       fontStyle: 'normal',
//       fontWeight: '400',
//       fontSize: 19,
//       color: Theme.colors.black,
//       textAlign: 'center',
//       marginTop: '5.18%',
//       textShadowColor: 'rgba(0, 0, 0, 0.25)',
//       textShadowOffset: {width: 0, height: 4},
//       textShadowRadius: 4,
//     },
    

//     newUserText: {
//       fontFamily: Theme.FontFamily.normal,
//       fontStyle: 'normal',
//       fontWeight: '500',
//       fontSize: 12,
//       lineHeight: 15,
//       textAlign: 'center',
//       color: '#000',
//     },
//     createAccountStyles:{
      
//         fontFamily: Theme.FontFamily.normal,
//       fontStyle: 'normal',
//       fontWeight: '500',
//       fontSize: 12,
//       lineHeight: 15,
//       textAlign: 'center',
//       color: '#000',
//       paddingVertical:5
      
//     },
//     createAccountTextStyles:{
//       fontFamily: Theme.FontFamily.normal,
//     fontStyle: 'normal',
//     fontWeight: '500',
//     fontSize: 12,
//     lineHeight: 15,
//     textAlign: 'center',
//     color: '#BFA058',
//     paddingVertical:5
//     }
//   });
// };
// export default Style;



//import liraries
import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {useTheme} from '../../../constants/ThemeContext';
import {windowHeight} from '../../../constants/GlobalStyles';
import {Theme} from '../../../constants';

// create a component
const Style = () => {
  const {colorTheme} = useTheme;
  return StyleSheet.create({
    container: {
      flex: 1,
    },
    yellowLine: {
      width: '100%',
      height: 5,
      backgroundColor: Theme.colors.primary,
      borderColor: Theme.colors.black,
      borderWidth: 1,
      shadowColor: Theme.colors.black,
      shadowOffset: {width: 1, height: 3},
      shadowOpacity: 0.2,
      shadowRadius: 1,
    },
    containerBox: {
      backgroundColor: Theme.colors.white,
      height: windowHeight - 5,
      width: '94%',
      // borderTopColor: Theme.colors.white,
      // borderWidth: 1,
      alignSelf: 'center',
      elevation: 4,
      shadowColor: 'rgba(0, 0, 0, 0.25)',
      shadowOffset: {width: 0, height: 4},
      shadowOpacity: 1,
      shadowRadius: 4
    },
    loginText: {
      fontFamily: Theme.FontFamily.normal,
      fontStyle: 'normal',
      fontWeight: '400',
      fontSize: 14,
      color: Theme.colors.black,
      textAlign: 'center',
      marginTop:'10%'
    },
    

    newUserText: {
      fontFamily: Theme.FontFamily.normal,
      fontStyle: 'normal',
      fontWeight: '500',
      fontSize: 12,
      lineHeight: 15,
      textAlign: 'center',
      color: '#000',
    },
    createAccountStyles:{
      
        fontFamily: Theme.FontFamily.normal,
      fontStyle: 'normal',
      fontWeight: '500',
      fontSize: 12,
      lineHeight: 15,
      textAlign: 'center',
      color: '#000',
      paddingVertical:5
      
    },
    createAccountTextStyles:{
      fontFamily: Theme.FontFamily.normal,
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 12,
    lineHeight: 15,
    textAlign: 'center',
    color: '#BFA058',
    paddingVertical:5
    },
    doctorNameText:
      {
        fontFamily: Theme.FontFamily.normal,
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 17,
        lineHeight: 20,
        textAlign: 'center',
        color: '#BFA058',
        marginTop:5
      
    }
  });
};
export default Style;
