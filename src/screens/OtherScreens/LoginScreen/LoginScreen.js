import { View, Text, Dimensions, StatusBar, TouchableOpacity } from 'react-native';
import React, { useRef } from 'react';
import { Header, ScreenLayout } from '../../../Components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { SvgUri } from 'react-native-svg';

import LogoImage from '../../../assets/images/Login_Page_icon.svg';
import MobileIcon from '../../../assets/images/mobile_icon.svg';
import { Theme } from '../../../constants';
import { TextInput } from 'react-native-gesture-handler';
import Style from './Style';
import EditTextInputBox from '../../../Components/EditTextInputBox/EditTextInputBox';
import BlackButton from '../../../Components/BlackButton/BlackButton';
import { customBody_height } from '../../../constants/window';

const Styles = Style()

const LoginScreen = (props) => {
  const { colors, FontFamily, sizes } = Theme;

  /* For Header customization Animation */
  const viewRef = useRef(null);
  const showBounceAnimation = (value) => {
    value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
    value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
  }


  const goToRegistration = () => {
    alert("Create account button clicked")
  }

  const clickLoginSubmit = () => {
    alert("Submit button clicked")
  }


  return (

    <ScreenLayout
      isHeaderShown={false}
      isHeaderLogo={false}
      hTitle={"Home"}

      showHeaderLine={true}
      customBackground={Theme.colors.secondaryBackground}

      isBackBtn={true}
      backBtnIcnName={"chevron-back-outline"}
      backBtnFunc={() => {
        showBounceAnimation(viewRef)
      }}
      backButton_animationRef={viewRef}
    >
      <View
        style={Styles.containerBox}>
        <LogoImage style={{ alignSelf: 'center', marginTop: '30%' }} />
        <Text
          style={Styles.loginText}>
          Login to your account
        </Text>
        <EditTextInputBox keyboardType={'email-address'} style={{ marginTop: '20%' }} icon={<MobileIcon style={{ marginStart: '4%' }} />} placeHolderText="Enter your mobile number or email ID" />
        <BlackButton style={{ marginTop: '20%' }} buttonText="SUBMIT" onSubmitPress={()=>{props.navigation.navigate('LoginWithPin')}} />
        {/* <View style={{ flexDirection: 'row', position: 'absolute', bottom: 20, alignSelf: 'center', alignItems: 'center' }}>
          <Text style={Styles.newUserText}>New user ?  </Text>
          <TouchableOpacity
            style={Styles.createAccountStyles}
          >
            <Text
              style={Styles.createAccountTextStyles}
            >Create an acoount</Text>
          </TouchableOpacity>
        </View>
         */}
        <View style={{ flexDirection: 'row', marginTop:20, alignSelf: 'center', alignItems: 'center' }}>
          <Text style={Styles.newUserText}>New user ?  </Text>
          <TouchableOpacity
            style={Styles.createAccountStyles}
            onPress={()=>{
              // props.navigation.replace('PersonalInformation')
              props.navigation.replace("ChooseUserTypeScreen")
            }}
          >
            <Text
              style={Styles.createAccountTextStyles}
            >Create an acoount</Text>
          </TouchableOpacity>
        </View>

      </View>


    </ScreenLayout>


  );
};

export default LoginScreen;


/*

    // <SafeAreaView style={{backgroundColor: '#F8FBFF',flex:1,flexDirection:'column'}}>
    //   <View
    //     style={Styles.yellowLine}
    //   />

    //   <View
    //     style={Styles.containerBox}>
    //     <LogoImage style={{alignSelf: 'center', marginTop: '14%'}} />
    //     <Text
    //       style={Styles.loginText}>
    //       Login to your account
    //     </Text>

    //     <EditTextInputBox style={{marginTop:'20%'}} icon={<MobileIcon style={{marginStart: '4%'}} />} placeHolderText="Enter your mobile number or email ID"/>

    //   <BlackButton style={{marginTop:'20%'}} buttonText="SUBMIT"/>

    //   <View style={{flexDirection:'row',position:'absolute',bottom:0,alignSelf:'center',alignItems:'center'}}>
    //     <Text style={Styles.newUserText}>New user?</Text>
    // <TouchableOpacity
    // style={Styles.createAccountStyles}
    // >
    //   <Text
    //   style={Styles.createAccountTextStyles}
    //   >Create an acoount</Text>
    // </TouchableOpacity>
    //   </View>
    //   </View>
    // </SafeAreaView>

    */
