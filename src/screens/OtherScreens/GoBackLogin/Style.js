import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import { ScreenLayout } from '../../../Components'
import { Theme } from '../../../constants'
import { windowHeight } from '../../../constants/GlobalStyles'

const Style = () => {
  return StyleSheet.create({
    containerBox: {
        backgroundColor: Theme.colors.white,
        height: windowHeight - 5,
        width: '94%',
        alignSelf: 'center'
      },
      text: {
        fontFamily: Theme.FontFamily.normal,
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 15,
        lineHeight: 17,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        letterSpacing: -0.333333,
        color: Theme.colors.black,
        marginHorizontal:'20%',
        marginTop:'10%'
      }
  })
}

export default Style