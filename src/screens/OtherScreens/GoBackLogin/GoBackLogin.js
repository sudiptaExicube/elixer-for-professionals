import { View, Text, Image } from 'react-native'
import React from 'react'
import { BlackButton, ScreenLayout } from '../../../Components'
import Style from './Style'
import { useRef } from 'react'
import { Theme } from '../../../constants'

const Styles=Style()

const GoBackLogin = () => {

    const viewRef = useRef(null);
  const showBounceAnimation = (value) => {
    value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
    value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
  }

  return (
    <ScreenLayout 
    isHeaderShown={true}
      isHeaderLogo={false}
      headerBackground={Theme.colors.white}
      headerStyle={{width: '94%',alignSelf: 'center'}}
      showHeaderLine={true}
      customBackground={Theme.colors.secondaryBackground}

      isBackBtn={true}
      backBtnIcnName={"chevron-back-outline"}
      backBtnFunc={() => {
        showBounceAnimation(viewRef)
      }}
      backButton_animationRef={viewRef}
    >
        <View style={Styles.containerBox}>
        <Image style={{alignSelf:'center',marginTop:'40%'}} source={require("../../../assets/images/clapping1.png")}/>
        <Text style={Styles.text}>Your Password has been reset successfully.</Text>
        <BlackButton style={{marginTop:'10%'}} buttonText="Go back to login"/>
        </View>
    </ScreenLayout>
  )
}

export default GoBackLogin