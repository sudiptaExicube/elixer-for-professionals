// import { View, Text, SafeAreaView } from 'react-native'
// import React from 'react'
// import IntroLoginIcon from '../../../assets/images/IntroLoginIcon.svg'
// import IntroLoginBackground from '../../../assets/images/IntroLoginBackground.svg'
// import { Theme } from '../../../constants'

// const IntroLogin = () => {
//   return (
//     <SafeAreaView style={{flex:1,backgroundColor:Theme.colors.white}}>
//       <IntroLoginIcon style={{alignSelf:'center',marginTop:50}}/>
//     </SafeAreaView>
//   )
// }

// export default IntroLogin




import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable,
  Dimensions
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../../constants';
import { BlackButton, ScreenLayout } from '../../../Components';
import Style from './Style';
import NotificationPermission from '../../../assets/images/notification_permission.svg'
import ArrowDownIcon from '../../../assets/images/arrow_down.svg'
import ArrowUpIcon from '../../../assets/images/arrow_up.svg'
import PointMarkIcon from '../../../assets/images/point_mark.svg'

import WhatsappIcon from '../../../assets/images/whatsapp_icon.svg'
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import IntroLoginIcon from '../../../assets/images/IntroLoginIcon.svg'

const IntroLogin = props => {
	const Styles = Style()
	// const onPress = () => HelperFunctions.sampleFunction('its working fine');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */





	return (

		<ScreenLayout
			isHeaderShown={false}
			isHeaderLogo={false}
			hTitle={'Help & Support center'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>

        <View style={{height:'100%',width:'100%'}}>
          <View style={{flex:1.5,width:'100%'}}>
            <View style={{flex:1,justifyContent:'center',alignSelf:'center'}}>
				<View>
					<IntroLoginIcon height={300} width={300} />
				</View>
            </View>
          </View>
          <LinearGradient colors={['#BFA058', '#EEEDDE']} style={{flex:1,width:'100%',borderTopLeftRadius:20,borderTopRightRadius:20,paddingHorizontal:20,justifyContent:'center'}}>
			<View>
				<Text style={{textAlign:'center',fontSize:Theme.sizes.h4,fontFamily:Theme.FontFamily.bold,color:Theme.colors.black}}>Welcome to {'\n'}Chhaya health</Text>
			</View>
			<View style={{marginTop:20}}>
				<Text style={{textAlign:'center',fontSize:Theme.sizes.h7,fontFamily:Theme.FontFamily.medium,color:Theme.colors.black}}>
				Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit
				</Text>
			</View>
			<View>
				<BlackButton style={{ height:45,marginTop:20 }} buttonText="LOGIN" onSubmitPress={()=>{props.navigation.replace('LoginScreen')}} />
			</View>

			<View style={{flexDirection:'row',justifyContent:'center',paddingTop:10}}>
				<View>
					<Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.black}}>New user? </Text>
				</View>
				<TouchableOpacity 
				onPress={()=>{
					// props.navigation.navigate("PersonalInformation")
					props.navigation.navigate("ChooseUserTypeScreen")
					
				}}
				style={{paddingLeft:5}}>
					<Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.primary}}>Create an account now </Text>
				</TouchableOpacity>
			</View>


          </LinearGradient>


        </View>
          


			{/* </View> */}


		</ScreenLayout>
	)
}

export default IntroLogin;