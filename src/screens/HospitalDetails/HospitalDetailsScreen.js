import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image, Platform
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
// import { Header, ScreenLayout } from '../../../Components';
import { FlatList } from 'react-native';
import Style from './Style';
import { BlackButton, ScreenLayout } from '../../Components';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import HospitalLocIcon from '../../assets/images/hospital_address.svg';
import * as Animatable from 'react-native-animatable';
// function HospitalDetailsScreen({ navigation }) {
const HospitalDetailsScreen = props => {
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */
	const hospitalIconImage = require('../../assets/images/hospital_icon.png');

	const [selectedIndex, setSelectedIndex] = useState(null);

	const teamDetails = [
		{ value: '8am to 12pm' },
		{ value: '12pm to 4pm' },
		{ value: '4pm to 8pm' },
		{ value: '8pm to 12am' },
		{ value: '12am to 4am' },
		{ value: '4am to 8am' }
	]
	const selectedColor = { borderColor: '#0B8B18', color: '#0B8B18' }
	const defaultColor = { borderColor: Theme.colors.grey, color: Theme.colors.grey }

	/* == Render menu item return function ==*/
	const renderItem = ({ item, index }) => (
		<TouchableOpacity style={{}} onPress={() => { setSelectedIndex(index) }}>
			<View style={[selectedIndex == index ? selectedColor : defaultColor, { padding: 5, paddingHorizontal: 8, borderWidth: 1, borderRadius: 50, margin: 5, justifyContent: 'center', flexDirection: 'column' }]}>
				<Text style={[selectedIndex == index ? selectedColor : defaultColor, { fontSize: Theme.sizes.h7, textAlign: 'center' }]}>{item?.value}</Text>
			</View>
		</TouchableOpacity>
	)


	const [selected, setSelected] = useState('');
	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Book Slot'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '95%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>

				<View style={{
					position: 'absolute', top: 0, height: WindowData.customBody_height, width: '95%', backgroundColor: Theme.colors.white, alignSelf: 'center'
					// paddingHorizontal: 15, paddingVertical: 10
				}}>
					<View
						style={{
							width: '100%',
							padding: 1, borderWidth: 0.5, borderColor: Theme.colors.grey,

						}}
					>
						<View style={{ flexDirection: 'row', paddingVertical: 10, paddingHorizontal: 10 }}>
							<View style={{ height: 80, width: 80, borderRadius: 5 }}>
								<Image source={require('../../assets/images/demo_profile_one.jpeg')} style={{ height: 79, width: 79, borderRadius: 5, resizeMode: 'cover', borderColor: Theme.colors.primary, borderWidth: 1 }} />
							</View>
							<View style={{ flex: 1, paddingLeft: 10, justifyContent: 'center' }}>
								<Text style={[Styles.itemText, { paddingBottom: 5, fontSize: Theme.sizes.h6, letterSpacing: 0.7 }]}>Hospital Name</Text>

								<View style={{ flexDirection: 'row' }}>
									<Text style={[Styles.itemText, { paddingBottom: 5, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>
										{/* Speciallity:  */}
										Description: 
										</Text>
									<Text style={[Styles.itemText, { paddingBottom: 5, fontSize: Theme.sizes.sm, color: Theme.colors.grey }]}> {props.route.params.item?.type}</Text>
								</View>
								{props.route.params?.item?.type == 'Consultation' ?
									<View style={{flexDirection:'row',justifyContent:'flex-start'}}>
										<Animatable.View
											animation={'pulse'}
											easing="ease-out"
											iterationCount="infinite"
											style={{ borderWidth: 1, borderRadius: 5, justifyContent: 'center', flexDirection: 'column', borderColor: Theme.colors.black,paddingHorizontal:5 }}>
											<Text style={[Styles.itemText, { fontSize: Theme.sizes.sm, color: Theme.colors.black, paddingHorizontal: 2, paddingVertical: 3, textAlign: 'center' }]}>{props.route.params.item?.mode} CONSULTATION</Text>
										{/* <Text style={[Styles.itemText, { fontSize: Theme.sizes.sm, color: Theme.colors.primary,paddingHorizontal:2,paddingVertical:3,textAlign:'center' }]}>OFFLINE</Text> */}
										</Animatable.View>
									</View>
									: null
								}

							</View>
							<View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
								<Icon name="heart-outline" size={25} onPress={() => { alert("heart clicked") }} />
							</View>

						</View>


					</View>

					<ScrollView style={{ flex: 1 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						<View style={{ paddingBottom: 10, borderBottomWidth: 0.8, borderBottomColor: '#E6E6E6', paddingHorizontal: 10 }}>
							<View style={{ paddingTop: 20 }}>
								<Text style={{ fontFamily: Theme.FontFamily.medium, color: Theme.colors.black }}>Available Dates</Text>
							</View>
							<View style={{ borderWidth: 1, borderRadius: 10, marginVertical: 10, padding: 5, borderColor: Theme.colors.primary }}>
								<Calendar
									onDayPress={day => {
										setSelected(day.dateString);
									}}
									markedDates={{
										[selected]: { selected: true, disableTouchEvent: true, selectedDotColor: 'orange' }
									}}
								/>
							</View>
						</View>

						<View style={{ paddingBottom: 10, paddingHorizontal: 10, borderBottomWidth: 0.8, borderBottomColor: '#E6E6E6', }}>
							<View style={{ paddingTop: 20, paddingBottom: 5 }}>
								<Text style={{ fontFamily: Theme.FontFamily.medium, color: Theme.colors.black }}>Available Time</Text>
							</View>
							<View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
								<FlatList
									// horizontal={true}
									data={teamDetails}
									renderItem={renderItem}
									// keyExtractor={(item, index) => item.id}
									keyExtractor={(item, index) => index}
									showsVerticalScrollIndicator={false}
									showsHorizontalScrollIndicator={false}
									contentContainerStyle={{ flexDirection: 'column' }}
									numColumns={3}
								/>
							</View>
						</View>

						<View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: '5%', paddingHorizontal: 10, }}>
							<View style={{ flex: 1 }}>
								<View>
									<Text style={{ fontFamily: Theme.FontFamily.medium, color: Theme.colors.black, letterSpacing: 0.6, fontSize: Theme.sizes.h5 }}>
										Location
									</Text>
								</View>
								<View style={{ flexDirection: 'row', flex: 1, paddingTop: 10 }}>
									<View style={{ justifyContent: 'flex-start' }}>
										<HospitalLocIcon height={18} width={18} />
									</View>
									<View style={{ flex: 1, paddingLeft: 5 }}>
										<Text style={{ fontFamily: Theme.FontFamily.medium, color: Theme.colors.grey, fontSize: Theme.sizes.sm }}>
											Akshya Nagar 1st Block 1st Cross, Rammurthy nagar, Bangalore-560016
										</Text>
									</View>
								</View>
							</View>
							<View style={{ height: 100, width: 100 }}>
								<Image
									source={require('../../assets/images/demo_map.png')}
									style={{
										height: 100, width: 100, resizeMode: 'cover',
									}}
								/>
							</View>

						</View>

						<View style={{ paddingBottom: 10, marginTop: '10%' }}>
							<BlackButton buttonText={'CONFIRM'} onSubmitPress={() => { props.navigation.replace('BookingConfirmScreen') }} style={{ height: 44 }} textColor={Theme.colors.primary} textCustomStyle={{ letterSpacing: 0.8 }} />
						</View>


					</ScrollView>
					{Platform.OS == 'ios' ?
						<View style={{ paddingBottom: '8%' }}></View>
						: null}
				</View>


			</View>


		</ScreenLayout>


	)
}

export default HospitalDetailsScreen;