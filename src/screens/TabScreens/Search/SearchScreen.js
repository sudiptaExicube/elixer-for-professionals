import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable,
	Platform
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../../constants';
import { BlackButton, ScreenLayout, EditTextInputBox } from '../../../Components';
import Style from './Style';
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';


// function SearchScreen({ navigation }) {
const SearchScreen = props => {
	const customArrowIcon = require('../../../assets/images/custom_right_arrow.png')
	const Styles = Style()
	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const activeStyle = { borderColor: Theme.colors.primary, color: Theme.colors.primary }
	const defaultStyle = { borderColor: Theme.colors.black, color: Theme.colors.black }
	const [selectedType, setSelectedType] = useState(null)

	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Search'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={false}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '93%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>
				<View style={{ position: 'absolute', top: 10, height: WindowData.customBody_height-55, width: '93%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingVertical: 10 }}>

					<View style={{ paddingBottom: 10, paddingHorizontal: 15, }}>
						<View>
							<EditTextInputBox style={{ backgroundColor: '#D9D9D924', height: 45, width: '100%', borderRadius: 5,fontFamily:Theme.FontFamily.normal }} rightIcon={<Icon name="search-outline" size={22} style={{ paddingRight: 7 }} onPress={()=>{props.navigation.navigate('SearchListScreen') }} />} placeHolderText="Search Here for hospitals" />
						</View>
						<View style={{ flexDirection: 'row', paddingTop: 20, flexWrap: 'wrap' }}>
							{/* <Icon name="locate-outline" color={Theme.colors.primary} size={20} />
							<View style={{justifyContent:'center'}}>
								<Text style={{color:Theme.colors.primary,fontFamily:Theme.FontFamily.medium,fontSize:Theme.sizes.sm,paddingLeft:5}}>
								Search by Speciality :
								</Text>
							</View> */}
							<View style={{ justifyContent: 'center' }}>
								<Text style={{ color: Theme.colors.primary, fontFamily: Theme.FontFamily.medium, fontSize: Theme.sizes.sm, paddingLeft: 5 }}>
									Search by Speciality :
								</Text>
							</View>
							<TouchableOpacity onPress={() => { setSelectedType('All') }}>
								<Text style={[selectedType == 'All' ? activeStyle : defaultStyle, { borderWidth: 1, paddingHorizontal: 8, paddingVertical: 4, borderRadius: 7, textAlign: 'center', marginHorizontal: 4, fontFamily: Theme.FontFamily.medium, fontSize: Theme.sizes.sm, marginVertical: 5 }]}>All</Text>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => { setSelectedType('Emergency') }}>
								<Text style={[selectedType == 'Emergency' ? activeStyle : defaultStyle, { borderWidth: 1, paddingHorizontal: 8, paddingVertical: 4, borderRadius: 7, textAlign: 'center', marginHorizontal: 4, fontFamily: Theme.FontFamily.medium, fontSize: Theme.sizes.sm, marginVertical: 5 }]}>Emergency</Text>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => { setSelectedType('ICU') }}>
								<Text style={[selectedType == 'ICU' ? activeStyle : defaultStyle, { borderWidth: 1, paddingHorizontal: 8, paddingVertical: 4, borderRadius: 7, textAlign: 'center', marginHorizontal: 4, fontFamily: Theme.FontFamily.medium, fontSize: Theme.sizes.sm, marginVertical: 5 }]}>ICU</Text>
							</TouchableOpacity>

						</View>

					</View>
					<View style={{ paddingHorizontal: 15, paddingTop: 10 }}>
						<Text
							style={{ fontFamily: Theme.FontFamily.bold, fontSize: Theme.sizes.h5, color: Theme.colors.black, fontWeight: '600' }}
						>Recent Search</Text>
					</View>

					<ScrollView style={{ flex: 1, paddingHorizontal: 15, paddingTop: 10 }}>
						<TouchableOpacity 
							onPress={()=>{props.navigation.navigate('SearchListScreen') }}
							style={{ flexDirection: 'row', marginBottom: 10 }}>
							<View style={{ flex: 1 }}>
								<Text style={{fontFamily:Theme.FontFamily.normal,fontSize:Theme.sizes.sm}}>Lorem Ipsum is simply dummy text </Text>
							</View>
							<View style={{paddingLeft:5}}>
								<Image source={customArrowIcon} style={{ height: 20, width: 20, resizeMode: 'cover' }} />
							</View>
						</TouchableOpacity>

						<TouchableOpacity 
							onPress={()=>{props.navigation.navigate('SearchListScreen') }}
							style={{ flexDirection: 'row', marginBottom: 10 }}>
							<View style={{ flex: 1 }}>
								<Text style={{fontFamily:Theme.FontFamily.normal,fontSize:Theme.sizes.sm}}>Lorem Ipsum is simply dummy text of the printing  </Text>
							</View>
							<View style={{paddingLeft:5}}>
								<Image source={customArrowIcon} style={{ height: 20, width: 20, resizeMode: 'cover' }} />
							</View>
						</TouchableOpacity>

						<TouchableOpacity 
							onPress={()=>{props.navigation.navigate('SearchListScreen') }}
							style={{ flexDirection: 'row', marginBottom: 10 }}>
							<View style={{ flex: 1 }}>
								<Text style={{fontFamily:Theme.FontFamily.normal,fontSize:Theme.sizes.sm}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry  </Text>
							</View>
							<View style={{paddingLeft:5}}>
								<Image source={customArrowIcon} style={{ height: 20, width: 20, resizeMode: 'cover' }} />
							</View>
						</TouchableOpacity>
						



					</ScrollView>
					{Platform.OS == 'ios'?
						<View style={{paddingBottom:'8%'}}></View>
					:null}
				</View>


			</View>


		</ScreenLayout>
	)
}

export default SearchScreen;