import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image, Platform
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
// import { useTheme } from '../../../constants/ThemeContext';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../../constants';
import { Header, ScreenLayout } from '../../../Components';
import { FlatList } from 'react-native';
import Style from './Style';
// import { Theme } from '../../../constants';

function HomeScreen({ navigation }) {
	// const globalstyles = GlobalStyles();
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	const [selectedIndex, setSelectedIndex] = useState('ongoing');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const teamDetails = [
		{ name: 'Pradip Mondal', type: 'Emergency', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: 'Sudipta Mukherjee', type: 'Consultation', mode: 'ONLINE', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: 'Pramit Paul', type: 'Consultation', mode: 'OFFLINE', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: 'Sudipta Mukherjee', type: 'Consultation',mode: 'ONLINE', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: 'Pramit Paul', type: 'Product lead', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: 'Pradip Mondal', type: 'Developer', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: 'Sudipta Mukherjee', type: 'ICU', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: 'Pramit Paul', type: 'Emergency', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: 'Sudipta Mukherjee', type: 'ICU', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: 'Pramit Paul', type: 'ICU', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" }
	]

	const completeDetails = [
		{ name: 'Pradip Mondal', type: 'Emergency', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: 'Sudipta Mukherjee', type: 'Consultation', mode: 'ONLINE', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: 'Pramit Paul', type: 'Consultation', mode: 'OFFLINE', imageUrl: require('../../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<TouchableOpacity style={{}} onPress={() => {
			// navigation.navigate('AppointmentDetailsScreen')
			navigation.navigate('AppointmentDetailsScreen', { item: item })
		}}>
			<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', paddingVertical: 10, width: '100%', height: 100, marginBottom: 20 }}>
				<View style={{ height: 95, width: 95, borderRadius: 5 }}>
					<Image source={item?.imageUrl} style={{ height: 94, width: 94, borderRadius: 5, resizeMode: 'cover', borderColor: Theme.colors.primary, borderWidth: 1 }} />
				</View>
				<View style={{ paddingHorizontal: 10 }}>
					<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.h6, flexWrap: 'wrap', color: Theme.colors.primary, fontFamily: Theme.FontFamily.bold }]}>Hospital Name</Text>

					<View style={{ flexDirection: 'row' }}>
						<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>
							{/* Speciallity:  */}
							Description:
						</Text>
						<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm,
							textShadowColor: 'rgba(0, 0, 0, 0.25)',
							textShadowOffset: {width: 0, height: 4},
							textShadowRadius: 4, }]}> {item?.mode} {item?.type}</Text>
					</View>

					<View style={{ flexDirection: 'row' }}>
						<View style={{ flexDirection: 'row' }}>
							<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Date: </Text>
							<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm }]}>dd-mm-yyyy</Text>
						</View>
						<View style={{ flexDirection: 'row', paddingLeft: 10 }}>
							<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Time: </Text>
							<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm }]}>4am-12pm</Text>
						</View>
					</View>

					<Text numberOfLines={2} style={[Styles.itemText, { paddingBottom: 5, fontSize: Theme.sizes.sm }]}>
						Address: Akshya Nagar 1st Block 1st Cross, Rammurthy nagar, Bangalore-560016
					</Text>
				</View>
			</View>

		</TouchableOpacity>
	)

	const activeStyle={borderColor:Theme.colors.black,backgroundColor:Theme.colors.black}
	const defaultStyle={borderColor:Theme.colors.grey}
	const activeTextStyle={color:Theme.colors.white}
	const defaultTextStyle={color:Theme.colors.grey}


	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			// hTitle={'Schedule Appointments'}
			hTitle={'My Appointments'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={false}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '95%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>

				<View style={{ position: 'absolute', top: 10, height: WindowData.customBody_height - 60, width: '95%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingHorizontal: 15, paddingVertical: 10 }}>
					{/* <Text
						style={{ fontFamily: Theme.FontFamily.medium, fontSize: Theme.sizes.h5, color: Theme.colors.black, paddingBottom: 10 }}
					>
						Your appointments
					</Text> */}

				<View style={{flexDirection:'row',paddingBottom:10}}>
					<TouchableOpacity 
						onPress={()=>{setSelectedIndex("ongoing")}}
						style={[selectedIndex == 'ongoing' ? activeStyle : defaultStyle,{borderWidth:1,borderRadius:6,flex:1,justifyContent:'center',alignItems:'center',paddingVertical:5,marginRight:10,paddingHorizontal:5}]}>
						<View style={{flexDirection:'column',justifyContent:'center'}}>
							<Text style={[selectedIndex == 'ongoing' ? activeTextStyle : defaultTextStyle,{fontSize:Theme.sizes.h7,letterSpacing:0.6}]}>ONGOING</Text>
						</View>
					</TouchableOpacity>
					<TouchableOpacity 
						onPress={()=>{setSelectedIndex("complete")}}
						style={[selectedIndex == 'complete' ? activeStyle : defaultStyle,{borderWidth:1,borderRadius:6,flex:1,justifyContent:'center',alignItems:'center',paddingVertical:5,marginRight:10,paddingHorizontal:5}]}>
						<View style={{flexDirection:'column',justifyContent:'center'}}>
							<Text style={[selectedIndex == 'complete' ? activeTextStyle : defaultTextStyle,{fontSize:Theme.sizes.h7,letterSpacing:0.6}]}>COMPLETE</Text>
						</View>
					</TouchableOpacity>
					{/* <View style={{flex:2}}></View> */}
				</View>


					<ScrollView style={{ flex: 1 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						<View>
							<FlatList
								horizontal={false}
								data={selectedIndex == 'ongoing' ? teamDetails : completeDetails }
								renderItem={renderItem}
								// keyExtractor={(item, index) => item.id}
								keyExtractor={(item, index) => index}
								showsVerticalScrollIndicator={false}
								showsHorizontalScrollIndicator={false}
							/>
						</View>
					</ScrollView>

					{Platform.OS == 'ios' ?
						<View style={{ paddingBottom: '8%' }}></View>
						: null}
				</View>


			</View>


		</ScreenLayout>


	)
}

export default HomeScreen