//import liraries
import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { Theme, WindowData } from '../../../constants';
// import { useTheme } from '../../Constants/Theme/Theme';


// create a component
const Styles = () => {
  // const {colorTheme} = useTheme();
  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingLeft:10,
      paddingRight:10
    },

    itemText:{
      fontFamily:Theme.FontFamily.medium,
      fontSize:Theme.sizes.h5,
      color:Theme.colors.black,
    },
    mainView:{
      width:'100%', alignSelf:'center',
    // height:WindowData.windowHeight-143,
      flex:1,
      paddingHorizontal:20
    },
    mainTouchableopacity:{
      borderRadius:10,paddingVertical:12,
      width:'100%',backgroundColor:'white',
      flex:1,flexDirection:'column',
      justifyContent:'center',marginBottom:15,
      shadowOffset:{width: 5, height: 10},
      shadowColor:'black',
      shadowRadius:10,elevation:5
    },

// ================================================

centeredView: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  // marginTop: 22
  paddingTop: 22,
},
modalView: {
  padding:15,
  marginLeft:30,
  marginRight:30,
  alignItems: 'center',
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.25,
  shadowRadius: 4,
  elevation: 5,
  backgroundColor:'white',
  // width:'80%',
  // width:360,
  height:320
},
button: {
  borderRadius: 20,
  padding: 10,
  elevation: 2,
},
buttonOpen: {
  backgroundColor: '#F194FF',
},
buttonClose: {
  backgroundColor: '#2196F3',
},
textStyle: {
  color: 'white',
  fontWeight: 'bold',
  textAlign: 'center',
},
modalText: {
  marginBottom: 15,
  textAlign: 'center',
},





  });

};
export default Styles;