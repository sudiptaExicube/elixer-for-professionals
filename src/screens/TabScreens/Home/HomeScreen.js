import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image, Platform
} from 'react-native';
import React, { useRef } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
// import { useTheme } from '../../../constants/ThemeContext';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../../constants';
import { BlackButton, Header, ScreenLayout } from '../../../Components';
import { FlatList } from 'react-native';
import Style from './Style';
// import { Theme } from '../../../constants';
// import CustomLocation from '../../../assets/images/custom_location.svg'

function HomeScreen({ navigation }) {
	// const globalstyles = GlobalStyles();
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	const customLocationIcon = require('../../../assets/images/custom_location.png')

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const teamDetails = [
		{ name: 'Lorem Ipsu', type: 'Emergency', designation: 'Developer', imageUrl: require('../../../assets/images/hospital_img1.png') },
		{ name: 'Lorem Ipsu', type: 'Consultation', mode: 'ONLINE', designation: 'Manager', imageUrl: require('../../../assets/images/hospital_img2.png')},
		{ name: 'Lorem Ipsu', type: 'Consultation', mode: 'OFFLINE', designation: 'Product lead', imageUrl: require('../../../assets/images/hospital_img3.png') },
		{ name: 'Lorem Ipsu', type: 'Emergency', designation: 'Developer', imageUrl: require('../../../assets/images/hospital_img1.png')},
		{ name: 'Lorem Ipsu', type: 'Emergency', designation: 'Manager', imageUrl: require('../../../assets/images/hospital_img2.png')},
		{ name: 'Lorem Ipsu', type: 'ICU', designation: 'Product lead', imageUrl: require('../../../assets/images/hospital_img3.png')},
		{ name: 'Lorem Ipsu', type: 'ICU', designation: 'Developer', imageUrl: require('../../../assets/images/hospital_img1.png')},
		{ name: 'Lorem Ipsu', type: 'Emergency', designation: 'Manager', imageUrl: require('../../../assets/images/hospital_img2.png')},
		{ name: 'Lorem Ipsu', type: 'Emergency', designation: 'Product lead', imageUrl: require('../../../assets/images/hospital_img3.png')},
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<View style={{}}>
			<View style={{ flex: 1, flexDirection: 'row', paddingVertical: 10, width: '100%', height: 100 }}>

				<View style={{ justifyContent: 'center', flexDirection: 'column' }}>
					<View style={{ height: 75, width: 75, borderRadius: 5, justifyContent: 'center', flexDirection: 'column' }}>
						<Image source={item?.imageUrl} style={{ height: 74, width: 74, borderRadius: 5, resizeMode: 'cover', borderColor: Theme.colors.primary, borderWidth: 1 }} />
					</View>
				</View>
				<View style={{ flex: 1, justifyContent: 'center', flexDirection: 'column' }}>
					<View style={{ paddingHorizontal: 10, justifyContent: 'center', flexDirection: 'column' }}>
						<Text style={[Styles.itemText, { fontFamily: Theme.FontFamily.bold, paddingBottom: 3, fontSize: Theme.sizes.h7, flexWrap: 'wrap', color: Theme.colors.primary, letterSpacing: 1.3 }]}>Hospital Name</Text>
						<Text numberOfLines={2} style={[Styles.itemText, { paddingBottom: 5, fontSize: Theme.sizes.sm, color: Theme.colors.black }]}>
							Address: Akshya Nagar 1st Block 1st Cross, Rammurthy nagar, Bangalore-560016
						</Text>
						<View style={{ flexDirection: 'row' }}>
							<Text style={[Styles.itemText, { paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.primary }]}>Description: </Text>
							<Text style={[Styles.itemText, {
								paddingBottom: 3, fontSize: Theme.sizes.sm, color: Theme.colors.black,
								textShadowColor: 'rgba(0, 0, 0, 0.25)',
								textShadowOffset: { width: 0, height: 4 },
								textShadowRadius: 4,
							}]}>{item?.mode} {item?.type}</Text>
						</View>
					</View>
				</View>
				<View style={{ justifyContent: 'center', flexDirection: 'column', paddingHorizontal: 5 }}>
					<BlackButton buttonText={'Book now'} onSubmitPress={() => {
						navigation.navigate('HospitalDetailsScreen', { item: item })
						console.log(item)
					}} style={{ height: 30, width: 70 }} textColor={Theme.colors.primary} textCustomStyle={{ fontSize: 10 }} />
				</View>

			</View>

		</View>
	)

	const renderItem2 = ({ item }) => (
		<View style={{ paddingRight: 15 }}>
			<View style={{ height: 70, width: 70, borderRadius: 10 }}>
				<Image source={item?.imageUrl} style={{ height: 70, width: 70, borderRadius: 10, resizeMode: 'cover', borderColor: Theme.colors.primary, borderWidth: 1 }} />
			</View>
			<Text numberOfLines={2} style={{ width: 70, textAlign: 'center', fontSize: 10, paddingTop: 5, fontFamily: Theme.FontFamily.normal }}>Lorem Ipsum</Text>
		</View>
	)



	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Home'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={false}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 106, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '95%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>

				<View style={{ position: 'absolute', top: 10, height: WindowData.customBody_height - 60, width: '95%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingVertical: 10 }}>

					<View style={{ paddingBottom: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#F1F1F1', marginBottom: 20, paddingHorizontal: 15 }}>
						<View style={{ height: 70, width: 70, borderRadius: 70, alignSelf: 'center', justifyContent: 'center', flexDirection: 'row' }}>
							<Image source={require('../../../assets/images/doctor_profile.png')} style={{ height: 70, width: 70, borderRadius: 70, resizeMode: 'contain', borderColor: Theme.colors.primary, borderWidth: 2, }} />
							<View style={{ position: 'absolute', bottom: 0, right: -3 }}>
								<View style={{ height: 25, width: 30, borderRadius: 5, backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignSelf: 'center' }}>
									<Image source={require('../../../assets/images/cameraIcon.png')} style={{ height: 20, width: 20, resizeMode: 'cover', alignSelf: 'center' }} />
								</View>
							</View>
						</View>
						<View style={{ flex: 1, paddingLeft: 15 }}>
							<Text numberOfLines={2} style={{ fontSize: Theme.sizes.h6, paddingTop: 5, color: Theme.colors.black, fontFamily: Theme.FontFamily.bold }}>
								Hello Doctor {"<Firth name>"}
							</Text>
							<Text numberOfLines={2} style={{ fontSize: Theme.sizes.h6, color: Theme.colors.black, fontFamily: Theme.FontFamily.bold }}>
								Good Evening!
							</Text>
							<TouchableOpacity onPress={() => { navigation.navigate('ChangeLocationScreen') }} style={{ flexDirection: 'row' }}>
								<View style={{ flexDirection: 'column', justifyContent: 'center' }}>
									<Image source={customLocationIcon} style={{ height: 18, width: 18, resizeMode: 'cover' }} />
								</View>
								<View style={{ flex: 1 }}>
									<Text numberOfLines={2} style={{ fontSize: 10, paddingTop: 5, paddingLeft: 5, color: Theme.colors.primary, fontFamily: Theme.FontFamily.normal }}>
										Akshya Nagar 1st Block 1st Cross, Rammurthy nagar, Bangalore-560016
									</Text>
								</View>
							</TouchableOpacity>
						</View>
						{/* <Text numberOfLines={2} style={{width:70,textAlign:'center',fontSize:10,paddingTop:5}}>Lorem Ipsum</Text> */}
					</View>

					<View style={{ paddingHorizontal: 15 }}>
						<FlatList
							horizontal={true}
							data={teamDetails}
							renderItem={renderItem2}
							// keyExtractor={(item, index) => item.id}
							keyExtractor={(item, index) => index}
							showsVerticalScrollIndicator={false}
							showsHorizontalScrollIndicator={false}
						/>
					</View>



					<Text
						style={{ paddingHorizontal: 15, fontFamily: Theme.FontFamily.medium, fontSize: Theme.sizes.h5, color: Theme.colors.black, paddingBottom: 10, paddingTop: 10 }}
					>
						Explore Hospitals
					</Text>

					<ScrollView style={{ flex: 1, paddingHorizontal: 15 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						<View>
							<FlatList
								horizontal={false}
								data={teamDetails}
								renderItem={renderItem}
								// keyExtractor={(item, index) => item.id}
								keyExtractor={(item, index) => index}
								showsVerticalScrollIndicator={false}
								showsHorizontalScrollIndicator={false}
							/>
						</View>
					</ScrollView>
					{Platform.OS == 'ios' ?
						<View style={{ paddingBottom: '8%' }}></View>
						: null}

				</View>


			</View>


		</ScreenLayout>


	)
}

export default HomeScreen