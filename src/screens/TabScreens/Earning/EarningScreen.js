// import {
// 	View,
// 	Text,
// 	TouchableOpacity,
// 	Switch
// } from 'react-native';
// import React from 'react'
// import Icon from 'react-native-vector-icons/Ionicons';
// import { useTheme } from '../../../constants/ThemeContext';
// import { GlobalStyles, HelperFunctions, Theme } from '../../../constants';


// function EarningScreen({ navigation }) {
// 	const styles = GlobalStyles();
// 	const onPress = () => HelperFunctions.sampleFunction('its working fine');
// 	// We're also pulling setScheme here!
// 	const { setScheme, isDark } = useTheme();
// 	const toggleScheme = () => {
// 		isDark ? setScheme('light') : setScheme('dark');
// 	}

// 	return (
// 		<View style={styles.container}>
// 			<TouchableOpacity
// 				onPress={onPress}
// 			>
// 				<Text style={styles.text}>Catagory Screen</Text>
// 			</TouchableOpacity>
// 		</View>
// 	)
// }

// export default EarningScreen

import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image, TouchableWithoutFeedback, Platform
} from 'react-native';
import React, { useRef } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
// import { useTheme } from '../../../constants/ThemeContext';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../../constants';
import { BlackButton, Header, ScreenLayout } from '../../../Components';
import { FlatList } from 'react-native';
import Style from './Style';
// import { Theme } from '../../../constants';
// import CustomLocation from '../../../assets/images/custom_location.svg'

// function EarningScreen({ navigation }) {
const EarningScreen = props =>{
	// const globalstyles = GlobalStyles();
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	const customLocationIcon = require('../../../assets/images/custom_location.png')

	/* For Header customization Animation */
	const viewRef = useRef(null);
	
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}

	const walletRef = useRef(null);
	const walletBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}

	/* For Header customization Animation END */

	const teamDetails = [
		{ name: '2023', designation: 'Developer', imageUrl: require('../../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: '2022', designation: 'Manager', imageUrl: require('../../../assets/images/hospital_img2.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: '2021', designation: 'Product lead', imageUrl: require('../../../assets/images/hospital_img3.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: '2020', designation: 'Developer', imageUrl: require('../../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" }
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<TouchableWithoutFeedback onPress={()=>{props.navigation.navigate('EarningDetailsScreen')}}>
			<View>
			<View style={{paddingBottom:10,paddingTop:10}}>
				<Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.black}}>Earning of {item.name}</Text>
			</View>
			<View style={{backgroundColor:'white',width:'100%',
						shadowColor: '#000',
						shadowOffset: { width: 0, height: 1 },
						shadowOpacity: 0.8,
						shadowRadius: 1, 
						elevation:2,borderRadius:5,
						flexDirection:'row',justifyContent:'space-between',padding:10,paddingHorizontal:15,marginBottom:15
						}}
				>
					<View style={{flex:1,height:50,justifyContent:'center',borderRightWidth:1,borderRightColor:'black',justifyContent:'space-evenly'}}>
						<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Total Day of work</Text>
						<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:'black'}}>30</Text>
					</View>
					<View style={{flex:1.2,height:50,justifyContent:'center',justifyContent:'space-evenly'}}>
						<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Total hours booked </Text>
						<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:'black'}}>240</Text>
					</View>
					<View style={{flex:1.2,height:50,justifyContent:'center',justifyContent:'space-evenly'}}>
						<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Total Earned</Text>
						<View style={{flexDirection:'row',alignSelf:'center'}}>
							<Image source={require('../../../assets/images/rupee-indian.png')} style={{ height: 12, width: 12, resizeMode: 'contain',justifyContent:'center',alignSelf:'center'}} />
							<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:Theme.colors.primary}}>115,000</Text>
						</View>
					</View>

				</View>
				</View>
		</TouchableWithoutFeedback>		
			)


	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Earning'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={false}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}

			icon1Name={'wallet'}
			icon1Click={()=>{
				walletBounceAnimation(walletRef);
				props.navigation.navigate('WalletScreen')
			}}
			icon1_animationRef={walletRef}
			icon1Color={Theme.colors.background_shade1}

		>
			<View style={Styles.mainView}>
				<View style={{ height: 70, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '95%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>

				<View style={{ position: 'absolute', top: 10, height: WindowData.customBody_height - 60, width: '95%',
				//  backgroundColor: Theme.colors.white,
				 alignSelf: 'center', paddingVertical: 10 }}>

					<View style={{ paddingBottom: 10, flexDirection: 'column', borderBottomWidth: 1, borderBottomColor: '#F1F1F1', marginBottom: 20, paddingHorizontal: 15 }}>
						<View style={{ height: 60, width: 60, borderRadius: 60, alignSelf: 'center', justifyContent: 'center', flexDirection: 'row' }}>
							<Image source={require('../../../assets/images/doctor_profile.png')} style={{ height: 60, width: 60, borderRadius: 60, resizeMode: 'contain', borderColor: Theme.colors.primary, borderWidth: 2, }} />
							<View style={{ position: 'absolute', bottom: 0, right: -3 }}>
								<View style={{ height: 25, width: 30, borderRadius: 5, backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignSelf: 'center' }}>
									<Image source={require('../../../assets/images/cameraIcon.png')} style={{ height: 20, width: 20, resizeMode: 'cover', alignSelf: 'center' }} />
								</View>
							</View>
						</View>
						<View style={{ }}>
							<Text numberOfLines={2} style={{ fontSize: Theme.sizes.h6, paddingTop: 5, color: Theme.colors.black, fontFamily: Theme.FontFamily.bold,textAlign:'center' }}>
								Hello Doctor {"<Firth name>"}
							</Text>
						</View>
					</View>

					<View>
						<View style={{backgroundColor:'white',width:'100%',
								shadowColor: '#000',
								shadowOffset: { width: 0, height: 1 },
								shadowOpacity: 0.8,
								shadowRadius: 1, 
								elevation:5,borderRadius:5,
								flexDirection:'row',justifyContent:'space-between',padding:10,paddingHorizontal:15,marginBottom:15
								}}
						>
							<View style={{flexDirection:'row',flex:1}}>
								<View style={{borderWidth:2,borderColor:Theme.colors.primary,height: 35, width: 35,justifyContent:'center',alignSelf:'center'}}>
									<Image source={require('../../../assets/images/clock_icon.png')} style={{ height: 35, width: 35, resizeMode: 'contain',justifyContent:'center',alignSelf:'center'}} />
								</View>
								<View style={{justifyContent:'center',paddingLeft:10,flex:1}}>
									<Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.black,}}>Total hours booked on app</Text>
								</View>
							</View>
							<View style={{justifyContent:'center'}}>
								<Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.primary}}>120 hours</Text>
							</View>
						</View>

						<View style={{backgroundColor:'white',width:'100%',
								shadowColor: '#000',
								shadowOffset: { width: 0, height: 1 },
								shadowOpacity: 0.8,
								shadowRadius: 1, 
								elevation:5,borderRadius:5,
								flexDirection:'row',justifyContent:'space-between',padding:10,paddingHorizontal:15,marginBottom:15
								}}
						>
							<View style={{flexDirection:'row',flex:1}}>
								<View style={{height: 35, width: 35,justifyContent:'center',alignSelf:'center'}}>
									<Image source={require('../../../assets/images/graph_icon.png')} style={{ height: 35, width: 35, resizeMode: 'contain',justifyContent:'center',alignSelf:'center'}} />
								</View>
								<View style={{justifyContent:'center',paddingLeft:10,flex:1}}>
									<Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.black,}}>Total earnings from app</Text>
								</View>
							</View>
							<View style={{justifyContent:'center',alignSelf:'center'}}>
									<Image source={require('../../../assets/images/rupee-indian.png')} style={{ height: 15, width: 15, resizeMode: 'contain',justifyContent:'center',alignSelf:'center'}} />
								</View>
							<View style={{justifyContent:'center'}}>
								<Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.black,paddingLeft:5}}>1500,000</Text>
							</View>
						</View>
						


					{/* <View style={{paddingBottom:10}}>
							<FlatList
								horizontal={false}
								data={teamDetails}
								renderItem={renderItem}
								// keyExtractor={(item, index) => item.id}
								keyExtractor={(item, index) => index}
								showsVerticalScrollIndicator={false}
								showsHorizontalScrollIndicator={false}
							/>
						</View> */}



					</View>



					<ScrollView style={{ flex: 1 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						<View>
							<FlatList
								horizontal={false}
								data={teamDetails}
								renderItem={renderItem}
								// keyExtractor={(item, index) => item.id}
								keyExtractor={(item, index) => index}
								showsVerticalScrollIndicator={false}
								showsHorizontalScrollIndicator={false}
							/>
						</View>
					</ScrollView>
					{Platform.OS == 'ios'?
						<View style={{paddingBottom:'8%'}}></View>
					:null}
				</View>


			</View>


		</ScreenLayout>


	)
}

export default EarningScreen;