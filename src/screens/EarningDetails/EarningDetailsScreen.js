import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image, TouchableWithoutFeedback, Platform
} from 'react-native';
import React, { useRef } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, Header, ScreenLayout } from '../../Components';
import { FlatList } from 'react-native';
import Style from './Style';

// function EarningDetailsScreen({ navigation }) {
const EarningDetailsScreen = props => {
	// const globalstyles = GlobalStyles();
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	const customLocationIcon = require('../../assets/images/custom_location.png')

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const teamDetails = [
		{ name: '20', designation: 'Developer', imageUrl: require('../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: '19', designation: 'Manager', imageUrl: require('../../assets/images/hospital_img2.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: '18', designation: 'Product lead', imageUrl: require('../../assets/images/hospital_img3.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: '14', designation: 'Developer', imageUrl: require('../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: '10', designation: 'Developer', imageUrl: require('../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: '07', designation: 'Developer', imageUrl: require('../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: '18', designation: 'Product lead', imageUrl: require('../../assets/images/hospital_img3.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: '14', designation: 'Developer', imageUrl: require('../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: '10', designation: 'Developer', imageUrl: require('../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: '07', designation: 'Developer', imageUrl: require('../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" }
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<TouchableWithoutFeedback onPress={()=>{props.navigation.navigate('IndividualEarningScreen')}}>
			<View>
				<View style={{
					backgroundColor: 'white', width: '100%',
					shadowColor: '#000',
					shadowOffset: { width: 0, height: 1 },
					shadowOpacity: 0.8,
					shadowRadius: 1,
					elevation: 2, borderRadius: 5,
					flexDirection: 'row', justifyContent: 'space-between', padding: 10, paddingHorizontal: 15, marginBottom: 15
				}}
				>
					<View style={{ flex: 1, height: 50, justifyContent: 'center', borderRightWidth: 1, borderRightColor: 'black', justifyContent: 'center' }}>
						<Text style={{ fontSize: 14, textAlign: 'center', fontFamily: Theme.FontFamily.bold, color: 'black' }}>{item.name}</Text>
						<Text style={{ fontSize: 9, textAlign: 'center', fontFamily: Theme.FontFamily.medium, color: Theme.colors.grey, paddingTop: 2 }}>May</Text>
					</View>
					<View style={{ flex: 2.5, height: 50, justifyContent: 'center', justifyContent: 'center' }}>
						<Text style={{ fontSize: 14, textAlign: 'center', fontFamily: Theme.FontFamily.bold, color: 'black' }}>8 hours</Text>
						<Text style={{ fontSize: 9, textAlign: 'center', fontFamily: Theme.FontFamily.medium, color: Theme.colors.grey, paddingTop: 2 }}>Of works </Text>
					</View>
					<View style={{ flex: 2.5, height: 50, justifyContent: 'center', justifyContent: 'center' }}>
						<View style={{ flexDirection: 'row', alignSelf: 'center' }}>
							<Image source={require('../../assets/images/rupee-indian.png')} style={{ height: 12, width: 12, resizeMode: 'contain', justifyContent: 'center', alignSelf: 'center' }} />
							<Text style={{ fontSize: 14, textAlign: 'center', fontFamily: Theme.FontFamily.bold, color: Theme.colors.primary, paddingTop: 2 }}>15,000</Text>
						</View>
						<Text style={{ fontSize: 9, textAlign: 'center', fontFamily: Theme.FontFamily.medium, color: Theme.colors.grey }}>Total Earned</Text>
					</View>

				</View>
			</View>
		</TouchableWithoutFeedback>
	)


	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Earning of 2022'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 50, width: '100%', backgroundColor: Theme.colors.primary }}>
				</View>

				<View style={{
					position: 'absolute', top: 10, height: WindowData.customBody_height, width: '95%',
					//  backgroundColor: Theme.colors.white,
					alignSelf: 'center', 
					// paddingVertical: 10
				}}>

					<View>
						<View style={{
							backgroundColor: 'white', width: '100%',
							shadowColor: '#000',
							shadowOffset: { width: 0, height: 1 },
							shadowOpacity: 0.8,
							shadowRadius: 1,
							elevation: 2, borderRadius: 5,
							flexDirection: 'row', justifyContent: 'space-between', padding: 10, paddingHorizontal: 15, marginBottom: 15
						}}
						>
							<View style={{flex:1,height:50,justifyContent:'center',borderRightWidth:1,borderRightColor:'black',justifyContent:'space-evenly'}}>
								<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Total Day of work</Text>
								<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:'black'}}>30</Text>
							</View>
							<View style={{flex:1.2,height:50,justifyContent:'center',justifyContent:'space-evenly'}}>
								<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Total hours booked </Text>
								<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:'black'}}>240</Text>
							</View>
							<View style={{flex:1.2,height:50,justifyContent:'center',justifyContent:'space-evenly'}}>
								<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Total Earned</Text>
								<View style={{flexDirection:'row',alignSelf:'center'}}>
									<Image source={require('../../assets/images/rupee-indian.png')} style={{ height: 12, width: 12, resizeMode: 'contain',justifyContent:'center',alignSelf:'center'}} />
									<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:Theme.colors.primary}}>115,000</Text>
								</View>
							</View>

						</View>
					</View>


					
				<View style={{paddingBottom:10}}>
					<Text style={{fontFamily:Theme.FontFamily.bold,color:Theme.colors.black,fontSize:Theme.sizes.h7}}>
						May 2022
					</Text>
				</View>



					<ScrollView style={{ flex: 1 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						<View>
							<FlatList
								horizontal={false}
								data={teamDetails}
								renderItem={renderItem}
								// keyExtractor={(item, index) => item.id}
								keyExtractor={(item, index) => index}
								showsVerticalScrollIndicator={false}
								showsHorizontalScrollIndicator={false}
							/>
						</View>
					</ScrollView>
					{Platform.OS == 'ios'?
						<View style={{paddingBottom:'8%'}}></View>
					:null}
				</View>


			</View>


		</ScreenLayout>


	)
}

export default EarningDetailsScreen;