// import {
// 	View,
// 	Text,
// 	TouchableOpacity,
// 	Switch,
// 	Pressable
// } from 'react-native';
// import React, { useRef, useState } from 'react'
// import Icon from 'react-native-vector-icons/Ionicons';
// import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
// import { BlackButton, ScreenLayout } from '../../Components';
// // import * as Animatable from 'react-native-animatable';
// // import Permission from '../../assets/images/permission.svg';
// import Style from './Style';
// // import { Rating, AirbnbRating } from 'react-native-ratings';
// // import { TextInput } from 'react-native';
// import NotificationPermission from '../../assets/images/notification_permission.svg'
// import MapPermission from '../../assets/images/map_permission.svg'
// import { ScrollView } from 'react-native';
// import { FlatList } from 'react-native';
// import { Image } from 'react-native';

// // function SetPasswordScreen({ navigation }) {
// const SetPasswordScreen = props =>{
// 	const Styles = Style()
// 	const onPress = () => HelperFunctions.sampleFunction('its working fine');

// 	// const [locationToggleValue, setLocationToggleValue] = useState(false);
// 	// const [notifiationToggleValue, setNotifiationToggleValue] = useState(false);

// 	/* For Header customization Animation */
// 	const viewRef = useRef(null);
// 	const viewRef1 = useRef(null);
// 	const viewRef2 = useRef(null);
// 	const viewRef3 = useRef(null);
// 	const showBounceAnimation = (value) => {
// 		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
// 		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
// 	}
// 	/* For Header customization Animation END */

// 	const teamDetails=[
// 		{name:'Pradip Mondal',designation:'Developer',imageUrl:require('../../assets/images/demo_profile_one.jpeg'),about:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb"},
// 		{name:'Sudipta Mukherjee',designation:'Manager',imageUrl:require('../../assets/images/demo_profile_one.jpeg'),about:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500"},
// 		{name:'Pramit Paul',designation:'Product lead',imageUrl:require('../../assets/images/demo_profile_one.jpeg'),about:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500"}
// 	]

// 	/* == Render menu item return function ==*/
// 	const renderItem = ({ item }) => (
// 		<View style={{}}>
// 			<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start',paddingVertical:10, width:160,height:230,marginHorizontal:10}}>
// 				<View style={{flexDirection:'row',justifyContent:'center'}}>
// 					<Image source={item?.imageUrl} style={{height:70,width:70,borderRadius:50,resizeMode:'cover'}} />
// 				</View>

// 				<View style={{ flexDirection: 'column', justifyContent: 'center',paddingTop:10 }}>
// 					<Text numberOfLines={2} style={[Styles.itemText,{textAlign:'center',paddingBottom:5,fontWeight: '600',fontSize:Theme.sizes.h6}]}>{item?.name}</Text>
// 					<Text numberOfLines={2} style={[Styles.itemText,{textAlign:'center',color:Theme.colors.primary,fontSize:Theme.sizes.h6}]}>{item?.designation}</Text>
// 				</View>
// 				<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center',paddingTop:10 }}>
// 					<Text ellipsizeMode='tail' numberOfLines={5} style={[Styles.itemText,{fontSize:10,textAlign:'center'}]}>{item?.about}</Text>
// 				</View>
// 			</View>
// 		</View>
// 	)





// 	return (

// 		<ScreenLayout
// 			isHeaderShown={true}
// 			isHeaderLogo={false}
// 			hTitle={'Set password'}
// 			// headerStyle={{backgroundColor:Theme.colors.primary}}
// 			// headerBackground={Theme.colors.primary}
//             headerBackground={'#F8FBFF'}
// 			showHeaderLine={false}
// 			// customBackground={Theme.colors.white}
// 			customBackground={'#F8FBFF'}
// 			isBackBtn={true}
// 			backBtnIcnName={"chevron-back-outline"}
// 			backBtnFunc={() => {
// 				showBounceAnimation(viewRef);
// 				navigation.goBack();

// 			}}
// 			backButton_animationRef={viewRef}
// 			showScrollView={true}
// 		>
// 			<View style={Styles.mainView}>


//                 <View style={{flex:1,width:'100%',backgroundColor:'red'}}>

//                 </View>


// 			</View>


// 		</ScreenLayout>
// 	)
// }

// export default SetPasswordScreen;


import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';

import Style from './Style';
import { BlackButton, EditTextInputBox, ScreenLayout } from '../../Components';
// import RedCloseIcon from '../../assets/images/close_icon_red.svg'
import CancelIcon from '../../assets/images/cancelled_icon.svg';
// import ResetPasswordLogo from '../../../assets/images/ResetPassword.svg'
import KeyIcon from '../../assets/images/Key.svg'

const SetPasswordScreen= props => {
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */
	const hospitalIconImage = require('../../assets/images/hospital_icon.png');
	
	const [selectedIndex, setSelectedIndex] = useState(null);
	const [iconName, setIconName] = useState('eye-outline');
	const [hidePassword, setHidePassword] = useState(true);


	const teamDetails = [
		{ value: '8am to 12pm' },
		{ value: '12pm to 4pm' },
		{ value: '4pm to 8pm' },
		{ value: '8pm to 12am' },
		{ value: '12am to 4am' },
		{ value: '4am to 8am' }
	]

	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={''}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={'#F8FBFF'}
            // headerBackground={'red'}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
            // customBackground={'red'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={false}
		>
			<View style={Styles.mainView}>
				<View style={{
                    height: WindowData.customBody_height, width: '90%', backgroundColor: Theme.colors.white, alignSelf: 'center',paddingTop:'40%'
				}}>
				
                    <View>
                        <View style={{paddingLeft:20,paddingBottom:30}}>
                            <Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.black}}>Set your 4 digit password</Text>
                        </View>
                        <View style={{marginBottom:30}}>
                            <EditTextInputBox secureTextEntry={hidePassword} style={{height:45,backgroundColor:'#D9D9D924'}} 
                                icon={<KeyIcon style={{marginStart: '4%',marginEnd:'1%'}}/>} 
                                placeHolderText="Set password"
                                rightIcon={<Icon name={iconName} size={20} style={{paddingRight:10}} 
								onPress={()=>{setIconName(iconName == 'eye-outline' ? 'eye-off-outline':'eye-outline');setHidePassword(!hidePassword) }} 
							/>}
                            />
                        </View>
                        <View style={{marginBottom:30}}>
                            <EditTextInputBox secureTextEntry={true} style={{height:45,backgroundColor:'#D9D9D924'}} icon={<KeyIcon style={{marginStart: '4%',marginEnd:'1%'}}/>} placeHolderText="Confirm password"/>
                        </View>
                        <View>
                            <BlackButton style={{height:45 }}  buttonText="SAVE" 
								
								// onSubmitPress={()=>{props.navigation.replace('CongratulationScreen')}}
								onSubmitPress={()=>{props.navigation.replace('IdentityVerification')}}
							/>
                        </View>
                    </View>

				</View>


			</View>


		</ScreenLayout>


	)
}

export default SetPasswordScreen;