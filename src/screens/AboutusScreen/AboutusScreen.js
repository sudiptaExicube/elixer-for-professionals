import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, ScreenLayout } from '../../Components';
// import * as Animatable from 'react-native-animatable';
// import Permission from '../../assets/images/permission.svg';
import Style from './Style';
// import { Rating, AirbnbRating } from 'react-native-ratings';
// import { TextInput } from 'react-native';
import NotificationPermission from '../../assets/images/notification_permission.svg'
import MapPermission from '../../assets/images/map_permission.svg'
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';

function AboutusScreen({ navigation }) {
	const Styles = Style()
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	// const [locationToggleValue, setLocationToggleValue] = useState(false);
	// const [notifiationToggleValue, setNotifiationToggleValue] = useState(false);

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const teamDetails=[
		{name:'Pradip Mondal',designation:'Developer',imageUrl:require('../../assets/images/demo_profile_one.jpeg'),about:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb"},
		{name:'Sudipta Mukherjee',designation:'Manager',imageUrl:require('../../assets/images/demo_profile_one.jpeg'),about:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500"},
		{name:'Pramit Paul',designation:'Product lead',imageUrl:require('../../assets/images/demo_profile_one.jpeg'),about:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500"}
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<View style={{}}>
			<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start',paddingVertical:10, width:160,height:230,marginHorizontal:10}}>
				<View style={{flexDirection:'row',justifyContent:'center'}}>
					<Image source={item?.imageUrl} style={{height:70,width:70,borderRadius:50,resizeMode:'cover'}} />
				</View>

				<View style={{ flexDirection: 'column', justifyContent: 'center',paddingTop:10 }}>
					<Text numberOfLines={2} style={[Styles.itemText,{textAlign:'center',paddingBottom:5,fontWeight: '600',fontSize:Theme.sizes.h6}]}>{item?.name}</Text>
					<Text numberOfLines={2} style={[Styles.itemText,{textAlign:'center',color:Theme.colors.primary,fontSize:Theme.sizes.h6}]}>{item?.designation}</Text>
				</View>
				<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center',paddingTop:10 }}>
					<Text ellipsizeMode='tail' numberOfLines={5} style={[Styles.itemText,{fontSize:10,textAlign:'center'}]}>{item?.about}</Text>
				</View>
			</View>
		</View>
	)





	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'About us'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{width:'90%',alignSelf:'center',backgroundColor:'white',height:100}}></View>
				</View>
				<View style={{ position: 'absolute', top: 0, height: WindowData.windowHeight-60, width: '90%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingHorizontal: 15, paddingVertical: 10 }}>
					<ScrollView style={{ flex: 1 }}>
						<View style={{paddingBottom:10}}>
							<Text 
								style={{fontFamily:Theme.FontFamily.medium,fontSize:Theme.sizes.h5,color:Theme.colors.black,fontWeight: '600'}}
							>Chhaya health app</Text>
						</View>
						<View>
							<Text 
								style={{lineHeight:17,fontFamily:Theme.FontFamily.medium,fontSize:Theme.sizes.h7,color:Theme.colors.black,fontWeight:'500'}}
							>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
							{'\n'}{'\n'} There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. 
							</Text>
						</View>

						<View style={{paddingTop:30}}>
							<Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.black,fontWeight: '600',fontSize:Theme.sizes.h5,paddingBottom:10}}>Our Team</Text>
							<View>
								<FlatList
								horizontal={true}
									data={teamDetails}
									renderItem={renderItem}
									// keyExtractor={(item, index) => item.id}
									keyExtractor={(item, index) => index}
									showsVerticalScrollIndicator={false}
									showsHorizontalScrollIndicator={false}
								// style={{paddingTop:30}}
								/>		
							</View>
											

						</View>
						
					</ScrollView>
				</View>


			</View>


		</ScreenLayout>
	)
}

export default AboutusScreen;