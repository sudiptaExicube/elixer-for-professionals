import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, ScreenLayout } from '../../Components';
import * as Animatable from 'react-native-animatable';
import Permission from '../../assets/images/permission.svg';
import Style from './Style';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { TextInput } from 'react-native';


function RatingScreen({ navigation }) {
	const Styles = Style()
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */
	const [reviewValue, setReviewValue] = React.useState('');

	const reviewButtonClick = () => {
		
	}
	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={''}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.white}
			showHeaderLine={false}
			customBackground={Theme.colors.white}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				navigation.goBack();

			}}
			backButton_animationRef={viewRef}
		>
			<View style={Styles.mainView}>
				<View style={{paddingTop:'7%'}}>
						<Animatable.Image 
							style={{ height: 70, width: 70 ,alignSelf:'center'}}
							source={require('../../assets/images/logo.png')}
							// animation={'zoomInUp'}
							animation={'pulse'}
							easing="ease-out" 
							iterationCount="infinite"
							>      
						</Animatable.Image>
						<View style={{paddingTop:'10%'}}>
							<Text style={{fontWeight:'600', fontFamily:Theme.FontFamily.medium,textAlign:'center',color:Theme.colors.black,fontSize:Theme.sizes.h3}}> Enjoyed Chhaya health? </Text>
						</View>

						<View style={{paddingTop:'12%'}}>
							<Text style={{fontWeight:'600', fontFamily:Theme.FontFamily.medium,textAlign:'center',fontSize:Theme.sizes.h6}}> How would you like to rate our service app? </Text>
						</View>
						<View style={{paddingTop:'18%'}}>
							<Rating
								type='custom'
								ratingCount={5}
								imageSize={35}
								selectedColor={Theme.colors.primaryColor}
								reviewColor={Theme.colors.primaryColor}
								startingValue={3}
							/>
						</View>

						<View style={{paddingTop:'20%'}}>
							<View><Text style={{fontFamily:Theme.FontFamily.medium,color:Theme.colors.black,fontWeight:'600'}}>Write a review(optional)</Text></View>
							<View style={{backgroundColor:'#F3F3F3',marginTop:10}}>
									<TextInput
										style={{paddingStart:15,color:Theme.colors.black,fontFamily:Theme.FontFamily.medium,color:Theme.colors.black,fontWeight:'600'}}
										// onChangeText={props.onChangeText}
										value={reviewValue}
										placeholder={'Text here..'}
										placeholderTextColor={Theme.colors.grey}
										underlineColorAndroid="transparent"
										numberOfLines={6}
										textAlignVertical={'top'}
										minHeight={120}
										onChangeText={(text) => {setReviewValue(text)}}

									/>
							</View>
						</View>

						<View style={{paddingTop:'8%'}}>
							<BlackButton style={{ height: 43,width:150, backgroundColor: Theme.colors.black }} buttonText="REVIEW NOW" onSubmitPress={() => { reviewButtonClick() }} />
						</View>
						
						<View style={{paddingTop:'5%'}}>
							<TouchableOpacity> 
								<Text style={{fontFamily:Theme.FontFamily.normal,color:Theme.colors.grey,textAlign:'center'}}>
									 May be later 
									 </Text> 
							</TouchableOpacity>
						</View>

						

				</View>


			</View>


		</ScreenLayout>
	)
}

export default RatingScreen;