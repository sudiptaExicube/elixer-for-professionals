import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image, TouchableWithoutFeedback, Platform
} from 'react-native';
import React, { useRef } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, Header, ScreenLayout } from '../../Components';
import { FlatList } from 'react-native';
import Style from './Style';
import IndEarningIcon from '../../assets/images/ind_earning_icon.svg';


// function IndividualEarningScreen({ navigation }) {
const IndividualEarningScreen = props => {
	// const globalstyles = GlobalStyles();
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	// const IndEarningIcon = require('../../assets/images/ind_earning_icon.svg')

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const teamDetails = [
		{ name: 'Name 1', designation: 'Developer', imageUrl: require('../../assets/images/hospital_img1.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: 'Name 2', designation: 'Manager', imageUrl: require('../../assets/images/hospital_img2.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: 'Name 3', designation: 'Product lead', imageUrl: require('../../assets/images/hospital_img3.png'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<View >
			<View style={{flexDirection:'row',paddingBottom:15}}>
				<View style={{flex:1.5,borderRightWidth:1,borderRightColor:'black',marginRight:10}}>
					<Text style={{fontSize:Theme.sizes.tiny,textAlign:'left',color:Theme.colors.black,paddingLeft:10,fontFamily:Theme.FontFamily.medium}}>{item?.name}</Text>
				</View>
				<View style={{flex:1,}}>
					<Text style={{fontSize:Theme.sizes.tiny,textAlign:'center',color:Theme.colors.black,fontFamily:Theme.FontFamily.medium}}>4</Text>
				</View>
				<View style={{flex:1,}}>
					<Text style={{fontSize:Theme.sizes.tiny,textAlign:'center',color:Theme.colors.black,fontFamily:Theme.FontFamily.medium}}>1500</Text>
				</View>
				<View style={{flex:1,}}>
					<Text style={{fontSize:Theme.sizes.tiny,textAlign:'center',color:Theme.colors.black,fontFamily:Theme.FontFamily.medium}}>₹ 15,000</Text>
				</View>

			</View>
		</View>
	)


	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Details'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={false}
		>
			<View style={[Styles.mainView,{paddingHorizontal:15,paddingVertical:10}]}>
				{/* <View style={{ height: 50, width: '100%', backgroundColor: Theme.colors.primary }}>
				</View> */}

				{/* <View style={{
					position: 'absolute', top: 10, height: WindowData.customBody_height, width: '95%',
					//  backgroundColor: Theme.colors.white,
					alignSelf: 'center', 
					// paddingVertical: 10
				}}>

					<View>
						<View style={{
							backgroundColor: 'white', width: '100%',
							shadowColor: '#000',
							shadowOffset: { width: 0, height: 1 },
							shadowOpacity: 0.8,
							shadowRadius: 1,
							elevation: 2, borderRadius: 5,
							flexDirection: 'row', justifyContent: 'space-between', padding: 10, paddingHorizontal: 15, marginBottom: 15
						}}
						>
							<View style={{flex:1,height:50,justifyContent:'center',borderRightWidth:1,borderRightColor:'black',justifyContent:'space-evenly'}}>
								<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Total Day of work</Text>
								<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:'black'}}>30</Text>
							</View>
							<View style={{flex:1.2,height:50,justifyContent:'center',justifyContent:'space-evenly'}}>
								<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Total hours booked </Text>
								<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:'black'}}>240</Text>
							</View>
							<View style={{flex:1.2,height:50,justifyContent:'center',justifyContent:'space-evenly'}}>
								<Text style={{fontSize:10,textAlign:'center',fontFamily:Theme.FontFamily.medium,color:'black'}}>Total Earned</Text>
								<View style={{flexDirection:'row',alignSelf:'center'}}>
									<Image source={require('../../assets/images/rupee-indian.png')} style={{ height: 12, width: 12, resizeMode: 'contain',justifyContent:'center',alignSelf:'center'}} />
									<Text style={{fontSize:14,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:Theme.colors.primary}}>115,000</Text>
								</View>
							</View>

						</View>
					</View>


					
				<View style={{paddingBottom:10}}>
					<Text style={{fontFamily:Theme.FontFamily.bold,color:Theme.colors.black,fontSize:Theme.sizes.h7}}>
						May 2022
					</Text>
				</View>



					<ScrollView style={{ flex: 1 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						<View>
							<FlatList
								horizontal={false}
								data={teamDetails}
								renderItem={renderItem}
								// keyExtractor={(item, index) => item.id}
								keyExtractor={(item, index) => index}
								showsVerticalScrollIndicator={false}
								showsHorizontalScrollIndicator={false}
							/>
						</View>
					</ScrollView>
				</View> */}


				<View style={{
					width:'100%',backgroundColor:Theme.colors.black,borderRadius:10,flexDirection:'row',paddingHorizontal:15,paddingVertical:20,
					}}
				>
					<View style={{flex:1,justifyContent:'center'}}>
							<Text style={{fontSize:Theme.sizes.h5,fontFamily:Theme.FontFamily.bold,color:Theme.colors.white,paddingBottom:5}}>Total Earning</Text>
							<Text style={{fontSize:Theme.sizes.h2,fontFamily:Theme.FontFamily.bold,color:Theme.colors.white}}> ₹ 15,000</Text>
					</View>
					<View style={{paddingHorizontal:10,justifyContent:'center'}}>
						<IndEarningIcon height={70} width={70} />
					</View>

				</View>

				<View style={{paddingTop:20}}>

					<View style={{paddingBottom:10}}>
						<Text style={{fontFamily:Theme.FontFamily.bold,fontSize:Theme.sizes.h7,color:Theme.colors.black}}>May 2022</Text>
					</View>
					<View>
						<View style={{
							backgroundColor: 'white', width: '100%',
							shadowColor: '#000',
							shadowOffset: { width: 0, height: 1 },
							shadowOpacity: 0.8,
							shadowRadius: 1,
							elevation: 5, borderRadius: 5,
							flexDirection: 'row', justifyContent: 'space-between', padding: 10, paddingHorizontal: 15, marginBottom: 15
						}}
						>
							<View style={{ flex: 1, height: 50, justifyContent: 'center', borderRightWidth: 1, borderRightColor: Theme.colors.grey, justifyContent: 'center' }}>
								<Text style={{ fontSize: 14, textAlign: 'center', fontFamily: Theme.FontFamily.bold, color: 'black',fontFamily:Theme.FontFamily.medium }}>25</Text>
								<Text style={{ fontSize: 9, textAlign: 'center', fontFamily: Theme.FontFamily.medium, color: Theme.colors.grey, paddingTop: 2,fontFamily:Theme.FontFamily.medium }}>May</Text>
							</View>
							<View style={{ flex: 2.5, height: 50, justifyContent: 'center', justifyContent: 'center' }}>
								<Text style={{ fontSize: 14, textAlign: 'center', fontFamily: Theme.FontFamily.bold, color: 'black',fontFamily:Theme.FontFamily.medium }}>8 hours</Text>
								<Text style={{ fontSize: 9, textAlign: 'center', fontFamily: Theme.FontFamily.medium, color: Theme.colors.grey, paddingTop: 2,fontFamily:Theme.FontFamily.medium }}>Of works </Text>
							</View>
							<View style={{ flex: 2.5, height: 50, justifyContent: 'center', justifyContent: 'center' }}>
								<View style={{ flexDirection: 'row', alignSelf: 'center' }}>
									<Image source={require('../../assets/images/rupee-indian.png')} style={{ height: 12, width: 12, resizeMode: 'contain', justifyContent: 'center', alignSelf: 'center' }} />
									<Text style={{ fontSize: 14, textAlign: 'center', fontFamily: Theme.FontFamily.bold, color: Theme.colors.primary, paddingTop: 2,fontFamily:Theme.FontFamily.medium }}>15,000</Text>
								</View>
								<Text style={{ fontSize: 9, textAlign: 'center', fontFamily: Theme.FontFamily.medium, color: Theme.colors.grey,fontFamily:Theme.FontFamily.medium }}>Total Earned</Text>
							</View>
						</View>
					</View>
				</View>

				<View >
						<View style={{flexDirection:'row',paddingVertical:15}}>
							<View style={{flex:1.5,marginRight:10}}>
								<Text style={{fontSize:Theme.sizes.tiny,textAlign:'left',color:Theme.colors.black,paddingLeft:10,fontFamily:Theme.FontFamily.medium}}>Hospital Name</Text>
							</View>
							<View style={{flex:1,}}>
								<Text style={{fontSize:Theme.sizes.tiny,textAlign:'center',color:Theme.colors.black,fontFamily:Theme.FontFamily.medium}}>Hour of work</Text>
							</View>
							<View style={{flex:1,}}>
								<Text style={{fontSize:Theme.sizes.tiny,textAlign:'center',color:Theme.colors.black,fontFamily:Theme.FontFamily.medium}}>Rate/ Hour</Text>
							</View>
							<View style={{flex:1,}}>
								<Text style={{fontSize:Theme.sizes.tiny,textAlign:'center',color:Theme.colors.black,fontFamily:Theme.FontFamily.medium}}>Total Earning</Text>
							</View>

						</View>
				</View>

				<View>
					<FlatList
						horizontal={false}
						data={teamDetails}
						renderItem={renderItem}
						// keyExtractor={(item, index) => item.id}
						keyExtractor={(item, index) => index}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					/>
				</View>
			</View>

			<View style={{backgroundColor:'white',width:'100%',paddingHorizontal:20,paddingVertical:20}}>
				<View style={{width:'100%',flexDirection:'row'}}>
					<View>	
						<Image source={require('../../assets/images/banknote_icon.png')} style={{ height: 35, width: 35, resizeMode: 'contain', justifyContent: 'center', alignSelf: 'center' }} />
					</View>
					<View style={{flex:1,paddingLeft:20,justifyContent:'center'}}>
						<Text style={{paddingBottom:10,color:Theme.colors.black,fontFamily:Theme.FontFamily.medium}}>Total amount disbursed</Text>
						<Text style={{color:Theme.colors.black,fontFamily:Theme.FontFamily.medium}}>Available Amount on the app</Text>
					</View>
					<View style={{justifyContent:'center'}}>
						<View style={{flexDirection:'row',alignSelf:'center',paddingBottom:10}}>
							<Image source={require('../../assets/images/rupee-indian.png')} style={{ height: 12, width: 12, resizeMode: 'contain',justifyContent:'center',alignSelf:'center'}} />
							<Text style={{fontSize:13,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:Theme.colors.black}}> 60,000</Text>
						</View>
						<View style={{flexDirection:'row',alignSelf:'center'}}>
							<Image source={require('../../assets/images/rupee-indian.png')} style={{ height: 12, width: 12, resizeMode: 'contain',justifyContent:'center',alignSelf:'center'}} />
							<Text style={{fontSize:13,textAlign:'center',fontFamily:Theme.FontFamily.bold,color:Theme.colors.black}}> 40,000</Text>
						</View>
					</View>

				</View>
				<View style={{paddingTop:15,flexDirection:'row',justifyContent:'flex-end'}}>
					<Text style={{fontSize:Theme.sizes.tiny,color:Theme.colors.grey}}>TDS applicable</Text>
				</View>


			</View>
			<View style={{height:20,width:'100%'}}></View>
			{Platform.OS == 'ios'?
				<View style={{paddingBottom:'8%'}}></View>
			:null}


		</ScreenLayout>


	)
}

export default IndividualEarningScreen;