import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { Theme } from '../../constants';
import { windowHeight } from '../../constants/GlobalStyles';


// create a component
const Style = () => {
//   const {colorTheme} = useTheme;
  return StyleSheet.create({
    containerBox: {
        backgroundColor: Theme.colors.white,
        height: windowHeight - 5,
        width: '94%',
        alignSelf: 'center'
      },
      otpText:{
        fontFamily: 'SFProText-Regular',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 17,
        letterSpacing: -0.333333,
        color: '#ABABAB',
        alignSelf:'center',
        marginStart:'2%',
        marginTop:5
      }
      
  })
}

export default Style;