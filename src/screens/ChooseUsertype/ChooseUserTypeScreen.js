import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, ScreenLayout } from '../../Components';
// import * as Animatable from 'react-native-animatable';
// import Permission from '../../assets/images/permission.svg';
import Style from './Style';
// import { Rating, AirbnbRating } from 'react-native-ratings';
// import { TextInput } from 'react-native';
import NotificationPermission from '../../assets/images/notification_permission.svg'
import MapPermission from '../../assets/images/map_permission.svg'
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';
import DoctorIcon from '../../assets/images/doctor_icon.svg'
import NurseIcon from '../../assets/images/nurse_icon.svg'

function ChooseUserTypeScreen(props) {
	const Styles = Style()
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	// const [locationToggleValue, setLocationToggleValue] = useState(false);
	// const [notifiationToggleValue, setNotifiationToggleValue] = useState(false);
	const [selectedType,setSelectedType]=useState(null)


	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const teamDetails = [
		{ name: 'Pradip Mondal', designation: 'Developer', imageUrl: require('../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500 csdkb cdsc b dscbdskc bdskjcbd skjcbdskj cbsdkbdskcb" },
		{ name: 'Sudipta Mukherjee', designation: 'Manager', imageUrl: require('../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" },
		{ name: 'Pramit Paul', designation: 'Product lead', imageUrl: require('../../assets/images/demo_profile_one.jpeg'), about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500" }
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<View style={{}}>
			<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start', paddingVertical: 10, width: 160, height: 230, marginHorizontal: 10 }}>
				<View style={{ flexDirection: 'row', justifyContent: 'center' }}>
					<Image source={item?.imageUrl} style={{ height: 70, width: 70, borderRadius: 50, resizeMode: 'cover' }} />
				</View>

				<View style={{ flexDirection: 'column', justifyContent: 'center', paddingTop: 10 }}>
					<Text numberOfLines={2} style={[Styles.itemText, { textAlign: 'center', paddingBottom: 5, fontWeight: '600', fontSize: Theme.sizes.h6 }]}>{item?.name}</Text>
					<Text numberOfLines={2} style={[Styles.itemText, { textAlign: 'center', color: Theme.colors.primary, fontSize: Theme.sizes.h6 }]}>{item?.designation}</Text>
				</View>
				<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', paddingTop: 10 }}>
					<Text ellipsizeMode='tail' numberOfLines={5} style={[Styles.itemText, { fontSize: 10, textAlign: 'center' }]}>{item?.about}</Text>
				</View>
			</View>
		</View>
	)





	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={''}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.white}
			showHeaderLine={true}
			headerlineBorder={'black'}
			headerlineBackground={'black'}
			// customBackground={Theme.colors.white}
			customBackground={Theme.colors.white}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				// props.navigation.goBack();
				props.navigation.replace('LoginScreen')

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={{paddingHorizontal:20}}>
				<View>
					<View style={{ paddingBottom: 10,paddingTop:'10%' }}>
							<Text
								style={{ fontFamily: Theme.FontFamily.bold, fontSize: Theme.sizes.h1, color: Theme.colors.primary, fontWeight: '600' }}
							>Hey,{"\n"}
							let’s Sign Up !</Text>
						</View>
				</View>
				<View>
					<View style={{ paddingTop: '25%',paddingBottom: 20}}>
						<Text
							style={{ fontFamily: Theme.FontFamily.bold, fontSize: Theme.sizes.h5, color: Theme.colors.black, fontWeight: '600' }}
						>Choose If you’re signing up as ... </Text>
					</View>

					<TouchableOpacity 
						onPress={()=>{
							setSelectedType('doctor');
							setTimeout(() => {
								props.navigation.navigate('PersonalInformation')
							}, 500);
						}}
						style={{flexDirection:'row',borderWidth:1,borderRadius:5,
						backgroundColor:selectedType == 'doctor' ? Theme.colors.primary : Theme.colors.background_shade1,
						borderColor:selectedType == 'doctor' ? Theme.colors.primary : Theme.colors.background_shade1,
						marginBottom:15}}>
						{selectedType == 'doctor' ? 
							<View style={{width:3,backgroundColor:'black',borderTopLeftRadius:5,borderBottomLeftRadius:5}}></View>
						:null}
						
						<View style={{padding:10,flexDirection:'row'}}>
							<View>
								<DoctorIcon height={30} width={30} />
							</View>
							<View style={{justifyContent:'center',paddingLeft:25}}>
								<Text style={{fontSize:Theme.sizes.h3,fontFamily:Theme.FontFamily.medium,
								color:selectedType == 'doctor' ? 'white' : Theme.colors.black
								}}>Doctor</Text>
							</View>
						</View>
					</TouchableOpacity>

					<TouchableOpacity 
						onPress={()=>{
							setSelectedType('nurse');
							setTimeout(() => {
								props.navigation.navigate('PersonalInformation')
							}, 500);
						}}
						style={{flexDirection:'row',borderWidth:1,borderRadius:5,
						backgroundColor:selectedType == 'nurse' ? Theme.colors.primary : Theme.colors.background_shade1,
						borderColor:selectedType == 'nurse' ? Theme.colors.primary : Theme.colors.background_shade1,
					}}>
						{selectedType == 'nurse' ? 
							<View style={{width:3,backgroundColor:'black',borderTopLeftRadius:5,borderBottomLeftRadius:5}}></View>
						:null}
						<View style={{padding:10,flexDirection:'row'}}>
							<View>
								<NurseIcon height={30} width={30} />
							</View>
							<View style={{justifyContent:'center',paddingLeft:25}}>
								<Text style={{fontSize:Theme.sizes.h3,fontFamily:Theme.FontFamily.medium,
								color:selectedType == 'nurse' ? 'white' : Theme.colors.black
								}}>Nurse</Text>
							</View>
						</View>
					</TouchableOpacity>

				</View>


			</View>


		</ScreenLayout>
	)
}

export default ChooseUserTypeScreen;