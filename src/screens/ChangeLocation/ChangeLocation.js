import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, ScreenLayout,EditTextInputBox } from '../../Components';
import Style from './Style';
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';


// function ChangeLocationScreen({ navigation }) {
const ChangeLocationScreen = props => {
	const customGpsIcon = require('../../assets/images/gps_icon.png')
	const Styles = Style()
	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */


	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Change Location'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 80, width: '100%', backgroundColor: Theme.colors.primary }}>
					<View style={{ width: '90%', alignSelf: 'center', backgroundColor: 'white', height: 100 }}></View>
				</View>
				<View style={{ position: 'absolute', top: 10, height: WindowData.customBody_height, width: '90%', backgroundColor: Theme.colors.white, alignSelf: 'center', paddingVertical: 10 }}>
					
					<View style={{paddingBottom:10,borderBottomWidth:1,borderBottomColor:'#F1F1F1',paddingHorizontal: 15,}}>
						<View>
							<EditTextInputBox style={{backgroundColor:'#D9D9D924',height:45,width:'100%',borderRadius:5}} placeHolderText="Search for area street name..."/>
						</View>
						<View style={{flexDirection:'row',paddingTop:5}}>
							{/* <Image source={customGpsIcon} style={{height:18,width:18,resizeMode:'cover'}} /> */}
							<Icon name="locate-outline" color={Theme.colors.primary} size={20} />
							<View style={{justifyContent:'center'}}>
								<Text style={{color:Theme.colors.primary,fontFamily:Theme.FontFamily.medium,fontSize:Theme.sizes.sm,paddingLeft:5}}>
								Set Current location using GPS
								</Text>
							</View>
						</View>

					</View>
					<View style={{ paddingHorizontal: 15,paddingTop:10  }}>
						<Text
							style={{ fontFamily: Theme.FontFamily.medium, fontSize: Theme.sizes.h5, color: Theme.colors.black, fontWeight: '600' }}
						>Saved Locations</Text>
					</View>

					<ScrollView style={{ flex: 1,paddingHorizontal: 15,paddingTop:10 }}>
						<View style={{flexDirection:'row',marginBottom:10}}>
							{/* <Image source={customGpsIcon} style={{height:18,width:18,resizeMode:'cover'}} /> */}
							<View style={{justifyContent:'center',}}>
								<Icon name="location-outline" color={Theme.colors.grey} size={25} />
							</View>
							<View style={{justifyContent:'center'}}>
								<Text style={{color:Theme.colors.grey,fontFamily:Theme.FontFamily.medium,fontSize:Theme.sizes.h6,paddingLeft:5}}>
								Akshya Nagar 1st Block 1st Cross, Rammurthy nagar, Bangalore-560016
								</Text>
							</View>
						</View>

					</ScrollView>
				</View>


			</View>


		</ScreenLayout>
	)
}

export default ChangeLocationScreen;