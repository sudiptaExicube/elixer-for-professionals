//import liraries
import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { Theme, WindowData } from '../../constants';
// import { useTheme } from '../../Constants/Theme/Theme';


// create a component
const Styles = () => {
  // const {colorTheme} = useTheme();
  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingLeft:10,
      paddingRight:10
    },

    itemText:{
      fontFamily:Theme.FontFamily.medium,
      fontSize:Theme.sizes.h5,
      color:Theme.colors.black,
    },
    mainView:{
      width:'100%', alignSelf:'center',
    // height:WindowData.windowHeight-143,
      flex:1,
      //paddingHorizontal:20
    },

    doctorName:{
      fontFamily: Theme.FontFamily.normal,
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: 15,
      lineHeight: 39,
      letterSpacing: -0.333333,
      textTransform: 'capitalize',
      color: Theme.colors.black,
      textAlign:'center',
      marginTop:30,
      textShadowColor: 'rgba(0, 0, 0, 0.25)',
      textShadowOffset: {width: 0, height: 2},
      textShadowRadius: 1,
        shadowOpacity:  0.17,
        elevation: 4,
      
    },
    text:{
      fontFamily: Theme.FontFamily.normal,
      fontStyle: 'normal',
      fontWeight: '400',
      fontSize: 13,
      lineHeight: 39,
      display: 'flex',
      alignItems: 'center',
      letterSpacing: -0.333333,
      textTransform: 'capitalize',
      color: Theme.colors.black
    }


  });

};
export default Styles;