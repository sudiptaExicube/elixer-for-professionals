import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	Pressable
} from 'react-native';
import React, { useRef, useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, ScreenLayout } from '../../Components';
// import * as Animatable from 'react-native-animatable';
// import Permission from '../../assets/images/permission.svg';
import Style from './Style';
// import { Rating, AirbnbRating } from 'react-native-ratings';
// import { TextInput } from 'react-native';
import NotificationPermission from '../../assets/images/notification_permission.svg'
import ArrowDownIcon from '../../assets/images/arrow_down.svg'
import ArrowUpIcon from '../../assets/images/arrow_up.svg'
import PointMarkIcon from '../../assets/images/point_mark.svg'

import WhatsappIcon from '../../assets/images/whatsapp_icon.svg'
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { Image } from 'react-native';

function HelpScreen({ navigation }) {
	const Styles = Style()
	const onPress = () => HelperFunctions.sampleFunction('its working fine');

	// const [locationToggleValue, setLocationToggleValue] = useState(false);
	// const [notifiationToggleValue, setNotifiationToggleValue] = useState(false);

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const [selectedIndex, setSelectedIndex] = useState(null);

	const teamDetails = [
		{ 
			name: 'My account', 
			subData:[
				{question:'How to change password?'},
				{question:'How to change profile Information?'},
				{question:'How to switch account from doctor to hospital / hospital to doctor?'}
			]
		},{ 
			name: 'Booking Information', 
			subData:[
				{question:'How to change password?'},
				{question:'How to change profile Information?'},
				{question:'How to switch account from doctor to hospital / hospital to doctor?'}
			]
		},{ 
			name: 'Payment Information', 
			subData:[
				{question:'How to change password?'},
				{question:'How to change profile Information?'},
				{question:'How to switch account from doctor to hospital / hospital to doctor?'}
			]
		},{ 
			name: 'How to use the app', 
			subData:[
				{question:'How to change password?'},
				{question:'How to change profile Information?'},
				{question:'How to switch account from doctor to hospital / hospital to doctor?'}
			]
		}
	]
	const onlyBorderTopRadius={borderTopLeftRadius:10,borderTopRightRadius:10};
	const allSideRadius={borderRadius:10}

	/* == Render menu item return function ==*/
	const renderItem = ({ item,index }) => (
		<View style={{}}>
			<View style={{marginBottom:10}}>
				<TouchableOpacity 
					onPress={()=>{selectedIndex == index ? setSelectedIndex(null):setSelectedIndex(index)}}
					style={[selectedIndex == index ?onlyBorderTopRadius :allSideRadius ,{height:50,width:'100%',backgroundColor:'#EFD9A8'}]}
				>
					<View style={{flexDirection:'row',justifyContent:'space-between',flex:1}}>
						<View style={{flex:1,flexDirection:'column',justifyContent:'center',paddingHorizontal:15}}>
							<Text style={{fontWeight: '600',fontFamily:Theme.FontFamily.medium,fontSize:Theme.sizes.h5,color:Theme.colors.black}}>{item?.name}</Text>
							</View>
						<View >
							<View style={{flexDirection:'column',justifyContent:'center',flex:1,paddingHorizontal:15}}>
								{selectedIndex == index ? 
									// <ArrowUpIcon height={17} width={17} />
									<Icon name ="chevron-up-outline" size={20} />
									:
									// <ArrowDownIcon height={17} width={17} />
									<Icon name ="chevron-down-outline" size={20} />
									
								}
								
							</View>
						</View>
					</View>

				</TouchableOpacity>
				{selectedIndex == index ? 
					<View>
					<FlatList
						// horizontal={true}
						data={item?.subData}
						renderItem={renderItem2}
						// keyExtractor={(item, index) => item.id}
						keyExtractor={(item, index) => index}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					// style={{paddingTop:30}}
					/>
				</View>
				:null}
			</View>
		</View>
	)


	const renderItem2 = ({ item }) => (
		<View>
			<View style={{paddingTop:10}}>
				<View style={{flexDirection:'row',justifyContent:'space-between',flex:1}}>
						<View>
							<View style={{flexDirection:'column',justifyContent:'flex-start',flex:1,paddingLeft:10}}>
								<PointMarkIcon height={15} width={15} />
							</View>
						</View>
						<View style={{flex:1,flexDirection:'column',justifyContent:'flex-start',paddingHorizontal:15,paddingLeft:7}}>
							<Text style={{fontWeight:'500',fontFamily:Theme.FontFamily.medium,fontSize:Theme.sizes.h7,color:Theme.colors.black}}>{item?.question}</Text>
						</View>
						
					</View>

				</View>
		</View>
	)




	return (

		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Help & Support center'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.primary}
			showHeaderLine={false}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={true}
		>
			<View style={Styles.mainView}>
				<View style={{ height: 30, width: '100%', backgroundColor: Theme.colors.primary }}></View>
				<View style={{ height: WindowData.customBody_height, width: '90%', backgroundColor: '#F8FBFF', alignSelf: 'center',paddingVertical:10 }}>
					<ScrollView
						style={{ flex: 1 }}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					>
						<View style={{  }}>
							<View>
								<FlatList
									// horizontal={true}
									data={teamDetails}
									renderItem={renderItem}
									// keyExtractor={(item, index) => item.id}
									keyExtractor={(item, index) => index}
									showsVerticalScrollIndicator={false}
									showsHorizontalScrollIndicator={false}
								// style={{paddingTop:30}}
								/>
							</View>
							<View style={{paddingTop:60}}>
								<View style={{flexDirection:'row',width:'100%',justifyContent:'space-between',paddingHorizontal:20,paddingVertical:30}}>
									<View style={{alignSelf:'center'}}><WhatsappIcon height={80} width={80} /></View>
									<View style={{flex:1,paddingLeft:20,alignSelf:'center',justifyContent:'center',flexDirection:'column'}}>
										{/* <View> */}
											<Text style={{textAlign:'center',fontFamily:Theme.FontFamily.medium,fontWeight: '600',color:Theme.colors.black,fontSize:Theme.sizes.h5,paddingbottom:10,letterSpacing:0}}>Didn’t found your solution?</Text>
											<Text style={{textAlign:'center',fontFamily:Theme.FontFamily.medium,fontWeight:'500',color:Theme.colors.black,fontSize:12,paddingBottom:10,paddingTop:5,letterSpacing:0}}>Whatsapp Us on this number</Text>
											<Text style={{textAlign:'center',fontFamily:Theme.FontFamily.bold,fontWeight: '600',color:Theme.colors.primary,fontSize:Theme.sizes.h6,paddingBottom:10,letterSpacing:0}}>+91 012 345 5678</Text>
										{/* </View> */}
									</View>


								</View>
							</View>


						</View>

					</ScrollView>
				</View>


			</View>


		</ScreenLayout>
	)
}

export default HelpScreen;