import {
	View,
	Text,
	TouchableOpacity,
	Switch,
	ScrollView, Image, TouchableWithoutFeedback, Platform
} from 'react-native';
import React, { useRef } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { GlobalStyles, HelperFunctions, Theme, WindowData } from '../../constants';
import { BlackButton, EditTextInputBox, Header, ScreenLayout } from '../../Components';
import { FlatList } from 'react-native';
import Style from './Style';
import IndEarningIcon from '../../assets/images/ind_earning_icon.svg';
import * as Animatable from 'react-native-animatable';
import { Modal } from 'react-native';
import { useState } from 'react';
import SignoutIcon from '../../assets/images/signout_icon.svg';
import CloseSvgIcon from '../../assets/images/close_icon.svg';
import CustomMenuIcon from '../TabScreens/Menu/MenuArray';


// function IndividualEarningScreen({ navigation }) {
const WalletScreen = props => {
	// const globalstyles = GlobalStyles();
	const Styles = Style()
	const globalstyles = GlobalStyles();
	const onPress = () => HelperFunctions.sampleFunction('its working fine');
	/* === Modal === */
	const [modalVisible, setModalVisible] = useState(false);

	// const IndEarningIcon = require('../../assets/images/ind_earning_icon.svg')

	/* For Header customization Animation */
	const viewRef = useRef(null);
	const viewRef1 = useRef(null);
	const viewRef2 = useRef(null);
	const viewRef3 = useRef(null);
	const showBounceAnimation = (value) => {
		value.current.animate({ 0: { scale: 1, rotate: '0deg' }, 1: { scale: 1.7, rotate: '0deg' } });
		value.current.animate({ 0: { scale: 1.7, rotate: '0deg' }, 1: { scale: 1, rotate: '0deg' } });
	}
	/* For Header customization Animation END */

	const teamDetails = [
		{ name: 'Name 1', designation: 'Developer', imageUrl: require('../../assets/images/hospital_img1.png'), wStatus:'Processing'  },
		{ name: 'Name 2', designation: 'Manager', imageUrl: require('../../assets/images/hospital_img2.png'),wStatus:'Processing'  },
		{ name: 'Name 3', designation: 'Product lead', imageUrl: require('../../assets/images/hospital_img3.png'), wStatus:'Complete' },
		

		
	]

	/* == Render menu item return function ==*/
	const renderItem = ({ item }) => (
		<TouchableWithoutFeedback onPress={()=>{}}>
		<View>
			<View 
			style={{
				backgroundColor: 'white', width: '100%',
				// shadowColor: '#000',
				// shadowOffset: { width: 0, height: 1 },
				// shadowOpacity: 0.8,
				// shadowRadius: 1,
				// elevation: 2, borderRadius: 5,
				flexDirection: 'row', justifyContent: 'space-between', padding: 0, paddingHorizontal: 15,
				borderBottomWidth:1,borderBottomColor:Theme.colors.background_shade1
			}}
			>
				<View style={{ flex: 1, height: 50, justifyContent: 'center', justifyContent: 'center' }}>
					<Text style={{ fontSize: 13, textAlign: 'center', fontFamily: Theme.FontFamily.bold, color: 'black' }}>22 July 2023</Text>
				</View>
				<View style={{ flex: 2.5, height: 50, justifyContent: 'center', justifyContent: 'center' }}>
					<View style={{ flexDirection: 'row', alignSelf: 'flex-end' }}>
						<Image source={require('../../assets/images/rupee-indian.png')} style={{ height: 12, width: 12, resizeMode: 'contain', justifyContent: 'center', alignSelf: 'center' }} />
						<Text style={{ fontSize: 15, textAlign: 'center', fontFamily: Theme.FontFamily.bold, color: Theme.colors.primary, paddingTop: 2 }}>15,000</Text>
					</View>
					<Text style={{ fontSize: 9, textAlign: 'right', fontFamily: Theme.FontFamily.medium, color: item.wStatus=='Complete' ? 'green': Theme.colors.black,paddingTop:5 }}>{item?.wStatus}</Text>
				</View>
			</View>
		</View>
	</TouchableWithoutFeedback>
	
	
	)


	return (


		<ScreenLayout
			isHeaderShown={true}
			isHeaderLogo={false}
			hTitle={'Wallet'}
			// headerStyle={{backgroundColor:Theme.colors.primary}}
			headerBackground={Theme.colors.black}
			showHeaderLine={false}
			headerTextStyle={{color:"white"}}
			// customBackground={Theme.colors.white}
			customBackground={'#F8FBFF'}
			isBackBtn={true}
			backBtnIcnName={"chevron-back-outline"}
			backBtnFunc={() => {
				showBounceAnimation(viewRef);
				props.navigation.goBack();

			}}
			backButton_animationRef={viewRef}
			showScrollView={false}
		>
			<View style={[Styles.mainView,{
				// paddingHorizontal:15,
				paddingVertical:10,paddingTop:0}]}>

				<View style={{}}>
					<View style={{
						backgroundColor:Theme.colors.black,
						// borderRadius:10,
						// height:180,
						width:'100%',flexDirection:'column',paddingHorizontal:15,
						justifyContent:'center', alignItems:'center',
						paddingVertical:30
						}}
					>
						<View style={{justifyContent:'center'}}>
								<Text style={{fontSize:Theme.sizes.h5,fontFamily:Theme.FontFamily.bold,color:Theme.colors.white,paddingBottom:5,textAlign:'center'}}>Wallet Balance</Text>
								<Animatable.Text
									// animation={'pulse'}
									// easing="ease-out" 
									animation={'fadeIn'}
									easing="ease-out" 
									// iterationCount="infinite"
									style={{fontSize:Theme.sizes.h2,fontFamily:Theme.FontFamily.bold,color:Theme.colors.white,textAlign:'center'}}
								>
								₹ 15,000
							</Animatable.Text>
						</View>
						<TouchableOpacity 
						onPress={()=>{setModalVisible(true)}}
						style={{flexDirection:'row',justifyContent:'center'}}>
							<Text style={{color:'white',borderWidth:1,borderColor:'white',paddingHorizontal:13,paddingVertical:7,marginTop:25,borderRadius:8,fontSize:Theme.sizes.sm,fontFamily:Theme.FontFamily.medium}}>WITHDRAW NOW</Text>
						</TouchableOpacity>

					</View>

				</View>


				<View style={{paddingTop:20}}>

					<View style={{paddingBottom:10,paddingHorizontal:20}}>
						<Text style={{fontFamily:Theme.FontFamily.bold,fontSize:Theme.sizes.h7,color:Theme.colors.black}}>Transaction History</Text>
					</View>
				</View>

				<Animatable.View animation={'fadeIn'}
									easing="ease-out" 
				>
					<FlatList
						horizontal={false}
						data={teamDetails}
						renderItem={renderItem}
						// keyExtractor={(item, index) => item.id}
						keyExtractor={(item, index) => index}
						showsVerticalScrollIndicator={false}
						showsHorizontalScrollIndicator={false}
					/>
				</Animatable.View>



				<View style={Styles.centeredView}>
					<Modal
						animationType="fade"
						// style={{backgroundColor: 'black'}}
						transparent={true}
						backdropOpacity={0.4}
						activeOpacity={0.6}
						visible={modalVisible}
						onRequestClose={() => {
							alert('Modal has been closed.');
							setModalVisible(!modalVisible);
						}}>
						<View style={[Styles.centeredView, { backgroundColor: 'rgba(0, 0, 0, 0.8)' }]}>
							<View style={Styles.modalView}>
								<View style={{flex:1}}>
									<View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: '100%' }}>
										<TouchableOpacity onPress={() => { setModalVisible(false) }}>
											<CustomMenuIcon svgName={CloseSvgIcon} height={17} width={17} />
										</TouchableOpacity>
									</View>
									<View style={{justifyContent:'center',flexDirection:'row'}}>
										<CustomMenuIcon svgName={SignoutIcon} height={90} width={90} />
									</View>
									<View style={{flex:2,justifyContent:'space-evenly'}}>
										<Animatable.View 
											animation={'fadeIn'}
											easing="ease-out" 
											// iterationCount="infinite"
											style={{flexDirection: 'row',paddingHorizontal:20}}>
											<Icon name='wallet' size={30} color={Theme.colors.primary} />
											<View style={{justifyContent:'center'}}>
												<Text style={{fontFamily:Theme.FontFamily.bold,size:Theme.sizes.h6,color:Theme.colors.primary}}> ₹ 15,000 </Text>
											</View>
										</Animatable.View>
										
										<EditTextInputBox keyboardType={'numeric'}  style={{height:45,backgroundColor:'#D9D9D924'}}  placeHolderText="Enter withdraw amount"/>
									</View>
									<View style={{flex:1,flexDirection:'row'}}>
										<View style={{flex:1}}></View>
										<View style={{ flex: 1,justifyContent:'center',flexDirection:'column' }}>
											<BlackButton style={{ height: 43, backgroundColor: Theme.colors.black }}
												textColor={Theme.colors.primary}
												textCustomStyle={{ fontWeight: '600', fontFamily: Theme.FontFamily.bold, fontSize: Theme.sizes.sm }}
												buttonText="Request now" onSubmitPress={() => { setModalVisible(false) }} />
										</View>
									</View>
								</View>

							</View>

						</View>

					</Modal>
				</View>




			</View>



			<View style={{height:20,width:'100%'}}></View>
			{Platform.OS == 'ios'?
				<View style={{paddingBottom:'8%'}}></View>
			:null}
		</ScreenLayout>


	)
}

export default WalletScreen;