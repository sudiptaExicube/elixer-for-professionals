import { View, Text } from 'react-native'
import React from 'react'
import BackIcon from '../../assets/images/back1.svg'
import Style from './Style'

const Styles=Style()

const BackHeader = (props) => {
  return (
    <View style={{flexDirection:'row',marginTop:'4%'}}>
    <BackIcon style={{marginStart:'4%'}}/>
      <Text style={Styles.backHeaderText}>{props.headText}</Text>
    </View>
  )
}

export default BackHeader