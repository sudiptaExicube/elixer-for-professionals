import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {useTheme} from '../../constants/ThemeContext'
import { Theme } from '../../constants';


// create a component
const Style = () => {
  const {colorTheme} = useTheme;
  return StyleSheet.create({
    inputBoxStyle: {
        borderWidth: 1,
        borderColor: '#BFA058',
        borderRadius: 10,
        width: '87%',
        height: '7%',
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center'
        
        
      },
      dropDownItem:{
        fontFamily: Theme.FontFamily.medium,
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 39,
        letterSpacing: -0.333333,
        textTransform: 'capitalize',
        color: Theme.colors.black,
        backgroundColor:'#fff'
      },
      text:{
        paddingStart:15,color:Theme.colors.black,fontSize:12
      }
  })
}

export default Style;
