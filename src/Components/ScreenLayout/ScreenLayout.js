import { useNavigation } from '@react-navigation/native';
import React, {Component, useRef} from 'react';
import {View, Text, StyleSheet, SafeAreaView, Alert, ScrollView} from 'react-native';
import { Theme, WindowData } from '../../constants';
import Header from '../Header/Header';
import Styles from './Style';

// create a component
const ScreenLayout = (props) => {
  const navigation = useNavigation();
  const styles = Styles()
  const {
    isHeaderShown,isHeaderLogo,hTitle, children, 
    backBtnFunc,backBtnIcnName,isBackBtn,customBackground,
    backButton_animationRef,
    icon1_animationRef,icon1Name,icon1Click,icon1Color,
    icon2_animationRef,icon2Name,icon2Click,
    icon3_animationRef,icon3Name,icon3Click,
    showHeaderLine,headerlineBorder,headerlineBackground,headerBackground,headerStyle,headerTextStyle,showScrollView
    } = props;

  return (
    <SafeAreaView style={[styles.mainContainer,{backgroundColor:customBackground ? customBackground : ''}]}>
      <View style={styles.container}>
        {showHeaderLine ? 
          <View style={{      
            width: '100%',
            height: 5,
            backgroundColor: headerlineBackground ? headerlineBackground : Theme.colors.primary,
            borderColor: headerlineBorder ? headerlineBorder :Theme.colors.black,
            borderWidth: headerlineBorder ? 1 :0,
            shadowColor: Theme.colors.black,
            shadowOffset: {width: 1, height: 3},
            shadowOpacity: 0.2,
            shadowRadius: 1}}
          ></View>
          :null
        }
        {isHeaderShown ? (
          <View style={{width: '100%'}}>            
            <Header 
            headerTextStyle={headerTextStyle}
            isShownHeaderLogo={isHeaderLogo}
            headerTitle={hTitle} 
            showBackButton={isBackBtn}
            backIconName={backBtnIcnName}
            backButtonClick={backBtnFunc}
            backButton_animationRef={backButton_animationRef}
            headerBackground={headerBackground}
            headerStyle={headerStyle}

            icon1_animationRef={icon1_animationRef}
            icon1Name={icon1Name}
            icon1Click={icon1Click}
            icon1Color={icon1Color}
            // icon2_animationRef={icon2_animationRef}
            // icon3_animationRef={icon3_animationRef}
            // icon2Name={icon2Name}
            // icon2Click={icon2Click}
            // icon3Name={icon3Name}
            // icon3Click={icon3Click}
            />
          </View>
        ) : (
          <></>
        )}
        <View
          style={[styles.childrenContainer, {
            // height: isHeaderShown ? WindowData.windowHeight-90 : WindowData.windowHeight, 
            flex:1,
            backgroundColor:customBackground ? customBackground : '' }]}
        >
          {!showScrollView ? 
            <ScrollView>
              {children}
            </ScrollView>
          :
            <View>
              {children}
            </View>
         }

        </View>
      </View>
    </SafeAreaView>
  );
};

//make this component available to the app
export default ScreenLayout;



/* === How to use this component ? ===== */
/*
		<ScreenLayout 
		isHeaderShown={true}
		isHeaderLogo={false}
		hTitle={"Home"}

    showHeaderLine={true}
    customBackground={Theme.colors.secondaryBackground}

		isBackBtn={true}
		backBtnIcnName={""}
		backBtnFunc={()=>{
			showBounceAnimation(viewRef)
		}}
		backButton_animationRef={viewRef}

		// icon1_animationRef={viewRef1}
		// icon2_animationRef={viewRef2}
		// icon3_animationRef={viewRef3}

		// icon1Name={'notifications-outline'}
		// icon1Click={()=>{
		// 	showBounceAnimation(viewRef1)
		// }}
		// icon2Name={'search-outline'}
		// icon2Click={()=>{
		// 	showBounceAnimation(viewRef2)
		// }}
		// icon3Name={'cart-outline'}
		// icon3Click={()=>{
		// 	showBounceAnimation(viewRef3)
		// }}
		/>

*/

