import { StyleSheet } from "react-native";
import { Theme } from "../../constants";
import { useTheme } from "../../constants/ThemeContext";

const Style = () => {
    
    return StyleSheet.create({
        yellowLine: {
            width: '100%',
            height: 5,
            backgroundColor: Theme.colors.primary,
            shadowColor: Theme.colors.black,
            shadowOffset: {width: 1, height: 3},
            shadowOpacity: 0.2,
            shadowRadius: 1,
          }
    })

}

export default Style;