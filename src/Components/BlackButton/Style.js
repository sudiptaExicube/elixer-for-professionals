import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { Theme } from '../../constants';
import {useTheme} from '../../constants/ThemeContext'


// create a component
const Style = () => {
  const {colorTheme} = useTheme;
  return StyleSheet.create({
    submitButtonStyle: {
        backgroundColor: '#000',
        height: '6%',
        width: '78%',
        borderRadius: 8,
        alignSelf: 'center',
        shadowColor: '#000',
  
        shadowOffset: {
          width: 0,
          height: 4,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 4,
        justifyContent: 'center',
      },
      submitTextStyle: {
        fontFamily: Theme.FontFamily.normal,
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 15,
        lineHeight: 16,
        textAlign: 'center',
        textTransform: 'uppercase',
        // color: '#BFA058',
      }
  })
}

export default Style;
