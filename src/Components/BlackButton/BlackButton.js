import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import Style from './Style'
import { Theme } from '../../constants'

const Styles=Style()

const BlackButton = (props) => {
  return (
    <TouchableOpacity 
      style={
      [Styles.submitButtonStyle,props.style]
      }
      onPress={props.onSubmitPress}
      >
        <Text style={[Styles.submitTextStyle,props.textCustomStyle,{color:props.textColor? props.textColor : Theme.colors.primary}]}>{props.buttonText}</Text>
      </TouchableOpacity>
  )
}

export default BlackButton