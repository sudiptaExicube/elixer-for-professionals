import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  token: 'csdcsdcd',
  isDarkMode:false
};

export const commonReducer = createSlice({
  name: 'common',
  initialState,
  reducers: {
    setIsDarkMode:(state,action)=>{
      state.isDarkMode = action.payload;
    },
    setToken: (state, action) => {
      state.token = action.payload;
    },
    resetToken: state => {
      state.token = '';
    },
    resetCommonStore: state => {
      state.token = '';
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  setIsDarkMode,
  setToken, 
  resetToken, 
  resetCommonStore
} = commonReducer.actions;

export default commonReducer.reducer;
