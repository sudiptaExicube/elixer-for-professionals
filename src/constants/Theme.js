const colors = {
  // primary : '#c5161d', 
  // secondary : '#087ea4',  //-- use rating point, small label, some of highlight part etc
  // white : '#fff',
  // black : '#000',
  // grey : '#d8d8d8',
  // light_grey : '#f0f0f0',
  // light_secondary:'#F2FAFC',
  // light_primary:'#FFF8F8'

  primary : '#BFA058', 
  secondary : '#9E8447',  //-- use rating point, small label, some of highlight part etc
  white : '#fff',
  black : '#0D0405',
  grey : '#ABABAB',
  background_shade1:'#E6E6E6'


  
};


// Light theme colors
const lightColors = {
  background: '#ffffff',
  // checklistColor: 'lightgrey',
  // homeBackground: 'white',
  // primary: '#4543D2',
  // text: '#121212',
  // placeholder: '#9B9EAA',
  // error: '#FF3B30',
  // tabBackground: '#F4F4F4',
  // cardBackground: '#E5E5E5',
  // textSecondary: '#121212',
  // textTertiary: '#000000',
  // border: '#E5E5E5',
  // buttonBackground: '#4543D2',
  // rightPaneRow: '#E5E5E5',
  // skeletonBg: '#E1E9EE',
  // skeletonHighlight: '#F2F8FC',
};

// Dark theme colors
const darkColors = {
  background: '#121212',
  // homeBackground: '#282c34',
  // checklistColor: '#7c829c',
  // primary: '#4543D2',
  // text: '#FFFFFF',
  // error: '#FF3B30',
  // tabBackground: '#222222',
  // cardBackground: '#373B4C',
  // placeholder: '#9B9EAA',
  // textSecondary: '#8A8C99',
  // textTertiary: '#CED2D9E5',
  // border: '#373B4C',
  // buttonBackground: '#373B4C',
  // rightPaneRow: '#585E77',
  // skeletonBg: '#10171E',
  // skeletonHighlight: '#15202B',
};




const sizes = {
  tiny: 10,
  sm: 12,
  h7: 13,
  h6: 14,
  h5: 16,
  h4: 18,
  h3: 20,
  h2: 22,
  h1: 24,
  large_title:32
};

const FontFamily = {
    // light: 'WorkSans-SemiBold',
    // normal: 'WorkSans-Regular',
    // medium: 'WorkSans-Medium',
    // bold: 'WorkSans-Bold'

    // light: 'sf-pro-text-semibold',
    // normal: 'sf-pro-text-regular',
    // medium: 'sf-pro-text-medium',
    // bold: 'sf-pro-text-bold'

    light: Platform.OS == 'ios'? 'HelveticaNeue-Light' : 'sf-pro-text-semibold',
    normal: Platform.OS == 'ios'? 'HelveticaNeue-Light' :'sf-pro-text-regular',
    medium: Platform.OS == 'ios'? 'HelveticaNeue-Medium' :'sf-pro-text-medium',
    bold: Platform.OS == 'ios'? 'HelveticaNeue-Medium' :'sf-pro-text-bold'


    /*
    const IOSStyle = {
    // height: Platform.OS = 'ios' ?  : 60,
    // position: 'absolute',
    elevation:0.8,
    // borderTopLeftRadius:7,borderTopRightRadius:7,
    backgroundColor:Theme.colors.light_primary
  }
  const AndStyle = {
    height: Platform.OS = 'ios' ?  : 60,
    // position: 'absolute',
    elevation:0.8,
    // borderTopLeftRadius:7,borderTopRightRadius:7,
    backgroundColor:Theme.colors.light_primary
  }
  */



    
};


const Theme = { colors, sizes, FontFamily, lightColors, darkColors };

export default Theme;